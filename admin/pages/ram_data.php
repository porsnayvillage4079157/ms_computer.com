<?php

require '../connection/config.php';

if (isset($_POST['get_data'])) {
    $queryData = "SELECT * FROM ram";
    $stetement = $con->query($queryData);
    $total_row = mysqli_num_rows($stetement);
    $table = '
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="10%">Image</th>
                <th width="5%">RAM Name</th>
                <th width="5%">RAM Size</th>
                <th width="20%">RAM Description</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        ';
    if ($total_row > 0) {
        $r = 1;
        while ($row = $stetement->fetch_assoc()) {
            $table .= '
        <tbody>
            <tr>
                <td>' . $r . '</td>
                <td>
                   <img src="' . ($row['ram_image']) . '" height="60" width="75" class="img-thumbnail" />
                </td>
                <td>' . $row["ram_name"] . '</td>
                <td>' . $row["ram_size"] . '</td>
                <td>' . $row["ram_description"] . '</td>
                <td>
                    <button type="button" data-id="' . $row["ram_id"] . '" class="btn btn-warning btn-xs" id="on_edit"><i class="fa fa-edit fa-fw" aria-hidden="true"></i>Edit</button>
                    <button type="button" data-id="' . $row["ram_id"] . '" class="btn btn-danger btn-xs" id="on_delete"><i class="fa fa-trash fa-fw" aria-hidden="true"></i>Delete</button>
                </td>
            </tr>
        </tbody>';
            $r++;
        }
    } else {
        $table .= '<tr class="alert alert-danger" colspan="4" align="center">Data not found</tr>';
    }
    $table .= '</table>';
    echo $table;
}

//INSERT

if (isset($_POST['ram_name_insert'])) {
    $ram_name = $_POST['ram_name_insert'];
    $ram_size = $_POST['ram_size'];
    $ram_description = $_POST['ram_description'];
    $image =  $_FILES['ram_image'];
    $location = 'upload/ram/' . basename($_FILES['ram_image']['name']);
    $extension = pathinfo($location, PATHINFO_EXTENSION);
    if ($extension != 'jpg' && $extension != 'jpag' && $extension != 'png') {
        echo 'Only jpg, jpag and png are allowed';
    } elseif ($_FILES['ram_image']['size'] > 2000000) {
        echo 'File is too big!';
    } elseif (file_exists($location)) {
        echo 'File is alreay uploaded!';
    } elseif (move_uploaded_file($_FILES['ram_image']['tmp_name'], $location)) {
        $insert = "INSERT INTO ram (ram_name,ram_size,ram_description,ram_image) VALUES ('$ram_name','$ram_size','$ram_description','$location')";
        mysqli_query($con, $insert);
        echo 'Data Save!';
    } else {
        echo 'Sometiong went wrong!';
    }
}

//edit

if (isset($_POST['get_edit'])) {
    $id = $_POST['up_id'];
    $selectData = "SELECT * FROM ram WHERE ram_id=$id";
    $execute = mysqli_query($con, $selectData);
    $data = $execute->fetch_assoc();
    echo json_encode($data);
}

// Update 

if (isset($_POST['up_ram_name'])) {
    $up_ram_name = $_POST['up_ram_name'];
    $up_ram_size = $_POST['up_ram_size'];
    $deleteImage = $_POST['old_image'];
    $up_ram_description = $_POST['up_ram_description'];
    $up_id = $_POST['up_id'];
    $locationUpdate = 'upload/ram/' . basename($_FILES['up_image']['name']);
    $imageUpdate =  $_FILES['up_image'];
    $extensionUpdate = pathinfo($locationUpdate, PATHINFO_EXTENSION);
    if ($extensionUpdate != 'jpg' && $extensionUpdate != 'jpag' && $extensionUpdate != 'png') {
        echo 'Only jpg, jpag and png are allowed';
    } elseif ($_FILES['up_image']['size'] > 2000000) {
        echo 'File is too big!';
    } elseif (file_exists($locationUpdate)) {
        echo 'File is alreay uploaded!';
    } elseif (move_uploaded_file($_FILES['up_image']['tmp_name'], $locationUpdate)) {
        $update = "UPDATE ram SET ram_name='$up_ram_name',ram_size='$up_ram_size',ram_description='$up_ram_description',ram_image='$locationUpdate' WHERE ram_id=$up_id";

        $result = mysqli_query($con, $update);
        if ($result) {
            unlink($deleteImage);
            echo 'Data save!';
        }
    } else {
        echo 'Sometiong went wrong!';
    }
}

// delete
if (isset($_POST['deleteData'])) {
    $getId = $_POST['deleteId'];
    $getData = "SELECT * FROM ram WHERE ram_id='$getId'";
    $executeData = mysqli_query($con, $getData);
    $deleteData = $executeData->fetch_assoc();
    echo json_encode($deleteData);
}

if (isset($_POST['on_delete'])) {
    $idDelete = $_POST['idDelete'];
    $pathImage = $_POST['imageDelete'];
    $scriptDelete = "DELETE FROM ram WHERE ram_id=$idDelete";
    $executeDelete = mysqli_query($con, $scriptDelete);
    unlink($pathImage);
}
