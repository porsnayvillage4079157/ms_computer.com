// --------------------------------------- Admin page -------------

//get all users

$(document).ready(function () {
    getUserData();
    update_user();
    get_update();
    on_delete();
});
function getUserData() {
    $.ajax({
        url: 'user_data.php',
        method: 'post',
        success: function (data) {
            $('#tbl_admin').html(data);
        }
    });

}
//Edit User
function update_user() {
    $(document).on('click', '#on_edit', function (e) {
        $('#modal_update_user').modal('show');
        var id = $(this).attr('data-id');
        var get_data = 'get-data';
        $.ajax({
            url: "signup.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                get_data: get_data,
                id: id,
            },
            success: function (data) {
                $('#update_modal').modal('show');
                $('#up_user_name').val(data.user_name);
                $('#data_old_password').val(data.user_password);
                $('#up_email').val(data.user_email);
                $('#old_image').val(data.user_image);
                $('#up_id').val(data.user_id);
                $('#image_display').html('<img style="border-radius: 50%;"; id="myImage" src="' + data.user_image + '" alt="" width="150px" height="150px">');

            }
        });

    });
}

function get_update() {
    $(document).on('submit', '#form_update_user', function (e) {
        e.preventDefault();
        var old_password = $('#old_password').val();
        var new_password = $('#new_password').val();
        var up_user_name = $('#up_user_name').val();
        var image = $('#up_image').val();
        console.log(old_password, new_password)
        if (old_password == '' || new_password == '' || up_user_name == '') {
            $('#update_message').html('Please fill in the blank!').fadeIn().fadeOut(5000);
            return false;
        } else if (old_password.length < 2 || new_password.length < 2 || up_user_name.length < 2) {
            $('#update_message').html('Please fill more than 2 character').fadeIn().fadeOut(5000);
            return false;

        } else {
            $.ajax({
                method: 'POST',
                url: "signup.php",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#update_message').html(response).fadeIn().fadeOut(4000);
                    getUserData();

                }
            });
        }


    });
}


// get delete

function on_delete() {
    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');
        var deleteData = 'delete';
        $.ajax({
            url: "signup.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                deleteData: deleteData,
                deleteId: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#delete_id').val(data.user_id);
                $('#delete_image').val(data.user_image);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.user_name + "</strong>?</h4>");
            }
        });

    });

    $('#btn_delete').click(function (e) {
        var idDelete = $('#delete_id').val();
        var imageDelete = $('#delete_image').val();
        console.log(imageDelete)
        var on_delete = 'on_delete';
        $.ajax({
            method: 'POST',
            url: "signup.php",
            data: {
                on_delete: on_delete,
                idDelete: idDelete,
                imageDelete: imageDelete,
            },
            success: function (response) {
                $('#delete_modal').modal('hide');
                getUserData();
            }
        });



    });
}


