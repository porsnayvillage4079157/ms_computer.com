<?php
require '../connection/config.php';

function getData($table, $id, $name, $name_select)
{
    global $con;
    $query = "SELECT * FROM $table";
    $execute = mysqli_query($con, $query);
    $total_row = mysqli_num_rows($execute);
    $output = '<select class="form-control text-center" name="' . $name_select . '">
                    <option value="">- - - Select- - -</option>';
    if ($total_row > 0) {
        while ($row = $execute->fetch_assoc()) {
            $output .= '<option value="' . $row[$id] . '">' . $row[$name] . '</option>';
        }
    } else {
        $output .= '<select>Data not found!</select>';
    }
    $output .= '</select>';
    echo json_encode($output);
}

if (isset($_POST['category']) || isset($_POST['up_category'])) {
    getData('category', 'category_id', 'category_name', 'category');
}

if (isset($_POST['os']) || isset($_POST['up_os'])) {
    getData('os', 'os_id', 'os_name', 'os');
}

if (isset($_POST['brand']) || isset($_POST['up_brand'])) {
    getData('brand', 'brand_id', 'brand_name', 'brand');
}

if (isset($_POST['processor']) || isset($_POST['up_processor'])) {
    getData('processor', 'processor_id', 'processor_name', 'processor');
}

if (isset($_POST['ram']) || isset($_POST['up_ram'])) {
    // getData('ram', 'ram_id', 'ram_name', 'ram');
    $sql = "SELECT * FROM ram";
    $execute_sql = mysqli_query($con, $sql);
    $total_row = mysqli_num_rows($execute_sql);
    $output = '<select class="form-control text-center" name="ram">
                    <option value="">- - - Select- - -</option>';
    if ($total_row > 0) {
        while ($row = $execute_sql->fetch_assoc()) {
            $output .= '<option value="' . $row['ram_id'] . '">' . $row['ram_name'] . ' - ' . $row['ram_size'] . 'GB.</option>';
        }
    } else {
        $output .= '<select>Data not found!</select>';
    }
    $output .= '</select>';
    echo json_encode($output);
}

if (isset($_POST['storage']) || isset($_POST['up_storage'])) {
    $scriptSelect = "SELECT storage.storage_id,storage.storage_size,storage_type.storage_type_name FROM storage LEFT JOIN storage_type ON storage.storage_type_id = storage_type.storage_type_id";

    $data = mysqli_query($con, $scriptSelect);
    $total_row = mysqli_num_rows($data);
    $output = '<select class="form-control" name="storage">
    <option value="">---Select---</option>';
    if ($total_row > 0) {
        while ($row = $data->fetch_assoc()) {
            $output .= '<option value="' . $row['storage_id'] . '">' . $row['storage_type_name'] . '-' . $row['storage_size'] . 'GB </option>';
        }
    } else {
        $output .= '<select>Data not found!</select>';
    }
    $output .= '</select>';
    echo json_encode($output);
}
