$(document).ready(function () {
    getData();
    on_delete();
});

// Get Data to tables pages

function getData() {
    $.ajax({
        url: 'contact_data.php',
        method: 'post',
        data: { table: 'table' },
        success: function (data) {
            $('#tbl_contact').html(data);
            // $('#dataTableResult').DataTable();
        }
    });

}

function on_delete() {

    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');

        var deleteData = 'delete';
        $.ajax({
            url: "contact_data.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                deleteData: deleteData,
                deleteId: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#delete_id').val(data.user_id);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.user_name + "</strong>?</h4>");
            }
        });
        $('#btn_delete').click(function (e) {
            var idDelete = $('#delete_id').val();
            var on_delete = 'on_delete';
            $.ajax({
                method: 'POST',
                url: "contact_data.php",
                data: {
                    on_delete: on_delete,
                    idDelete: idDelete,
                },
                success: function (response) {
                    $('#delete_modal').modal('hide');
                    getData();
                }
            });



        });

    });
}