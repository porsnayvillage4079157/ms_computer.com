<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark" id="navbar" role="navigation">
    <a class="navbar-brand" href="index.php"><img src="https://images.cooltext.com/5539052.png" width="200" height="40" alt="MS COMPUTER" /></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>

    </button>
    <div class="collapse navbar-collapse bg-dark" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active ml-4">
                <a class="nav-link" href="index.php"><i class="fa fa-home fa-fw" aria-hidden="true"></i> Home</a>
            </li>
            <li class="nav-item ml-4">
                <a class="nav-link" href="product_filter.php"><i class="fa fa-product-hunt fa-fw" aria-hidden="true"></i> Product</a>
            </li>
            <li class="nav-item ml-4">
                <a class="nav-link" href="about.php"><i class="fa fa-info fa-fw" aria-hidden="true"></i> About</a>
            </li>
            <li class="nav-item ml-4">
                <a class="nav-link" href="contact.php"><i class="fa fa-phone fa-fw" aria-hidden="true"></i> Contact</a>
            </li>
            <li class="nav-item ml-4">
                <a class="nav-link" href="../admin/pages/"><i class="fa fa-user-circle fa-fw" aria-hidden="true"></i> Admin</a>
            </li>
        </ul>
    </div>
</nav>