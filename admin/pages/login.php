<?php require '../connection/config.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/materia/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div class="message"></div>
        <div class="row">
            <div class="col-md-5 mx-auto mt-5">
                <div class="card">
                    <div class="card-header">
                        <h3>Login User</h3>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                                <input type="text" id="name" class="form-control name" name="name" placeholder="Name" require>
                                <div class="invalid-feedback errorName">Name is required</div>
                            </div>
                            <!-- Close form-group -->
                            <div class="form-group">
                                <input type="email" id="email" name="email" class="form-control email" placeholder="Email">
                                <div class="invalid-feedback errorEmail" require>Email is required</div>
                            </div>
                            <!-- Close form-group -->
                            <div class="form-group">
                                <input type="password" id="password" class="form-control password" placeholder="Password" name="password" require>
                                <div class="invalid-feedback errorPassword">Password is required</div>
                            </div>
                            <!-- Close form-group -->
                            <div class="form-group">
                                <button type="button" id="login" class="btn btn-info">Login &rarr;</button>
                            </div>
                            <!-- Close form-group -->
                        </form>
                    </div>
                    <!-- Close card-body -->
                </div>
                <!-- Close card -->
            </div>
            <!-- Close col-md-5 -->
        </div>



        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

        <script src="../js/ajax/register.js"></script>
</body>

</html>


</div>