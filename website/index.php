<?php
require './config/config.php';
require './config/function/index.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MS COMPUTER</title>
    <link rel="stylesheet" href="./assets/css/home_style.css">

    <!-- font awesome  -->
    <!-- <link rel="stylesheet" href="./assets/css/style.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script src="./assets/js/script.js"></script>
    <script src="./assets/js/carousel.js"></script>
    <style>
        .carousel-inner .active.left {
            background: white;
            left: -33%;
        }

        .carousel-indicators li {
            background: #757070;
        }

        .carousel-indicators li.active {
            background: red;
        }

        .carousel-inner .next {
            background: white;
            left: 33%;
        }

        .carousel-inner .prev {
            background: white;
            left: -33%;
        }

        .carousel-control.left,
        .carousel-control.right {
            background-image: none;
        }

        .carousel-multi .carousel-inner>.item {
            transition: 500ms ease-in-out left;
            background: white;
        }

        .carousel-multi .carousel-inner>.item>.media-card {
            background: white;
            background: #333;
            border-right: 10px solid #fff;
            display: table-cell;
            width: 1%;
            height: 400px;
        }

        .carousel-multi .carousel-inner>.item>.media-card:last-of-type {
            border-right: none;
            background: white;
        }

        .carousel-multi .carousel-inner .active {
            background: white !important;
            display: table;
        }

        .carousel-multi .carousel-inner .active.left {
            left: -33%;
        }

        .carousel-multi .carousel-inner .active.right {
            left: 33%;
        }

        .carousel-multi .carousel-inner .next {
            left: 33%;
        }

        .carousel-multi .carousel-inner .prev {
            left: -33%;
        }

        @media all and (transform-3d),
        (-webkit-transform-3d) {
            .carousel-multi .carousel-inner>.item {
                transition: 500ms ease-in-out all;
                backface-visibility: visible;
                transform: none !important;
            }
        }
    </style>
</head>

<body>
    <nav class="navbar p-2 navbar-fixed-top bg-dark">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="glyphicon glyphicon-tint white"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="https://images.cooltext.com/5539052.png" width="200" height="40" alt="MS COMPUTER" /></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li class="nav-item active ml-4">
                        <a class="nav-link" href="index.php"><i class="fa fa-home fa-fw" aria-hidden="true"></i> Home</a>
                    </li>
                    <li class="nav-item ml-4">
                        <a class="nav-link" href="product_filter.php"><i class="fa fa-product-hunt fa-fw" aria-hidden="true"></i> Product</a>
                    </li>
                    <li class="nav-item ml-4">
                        <a class="nav-link" href="about.php"><i class="fa fa-info fa-fw" aria-hidden="true"></i> About</a>
                    </li>
                    <li class="nav-item ml-4">
                        <a class="nav-link" href="contact.php"><i class="fa fa-phone fa-fw" aria-hidden="true"></i> Contact</a>
                    </li>
                    <li class="nav-item ml-4">
                        <a class="nav-link" href="../admin/pages/"><i class="fa fa-user-circle fa-fw" aria-hidden="true"></i> Admin</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <?php $sql = "SELECT * FROM products LIMIT 5";
    $execute = $con->query($sql);
    ?>
    <div style="margin-top: 162px;margin-bottom:162px;" id="carousel-example-multi" class="carousel carousel-multi slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-multi" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-multi" data-slide-to="1"></li>
            <li data-target="#carousel-example-multi" data-slide-to="2"></li>
            <li data-target="#carousel-example-multi" data-slide-to="3"></li>
            <li data-target="#carousel-example-multi" data-slide-to="4"></li>
            <li data-target="#carousel-example-multi" data-slide-to="5"></li>
        </ol>

        <!-- Wrapper for slides -->
        <h3 class="text-primary bg-dark">Our Product</h3>
        <div class="carousel-inner" role="listbox">
            <div class="item active">

                <div class="media media-card">
                    <img src="./assets/images/iPad Pro 12.9-inch Chip M1 Wifi.jpeg" width="100%" height="100%" alt="">

                </div>
            </div>
            <?php while ($row = $execute->fetch_assoc()) : ?>
                <div class="item">

                    <div class="media media-card">
                        <!-- <div class="carousel-caption">
                            <h3><?php echo $row['product_name'] ?></h3>
                            <p class="text-danger"><?php echo number_format($row['product_price']) . '$'; ?></p>
                        </div> -->
                        <div class="text-center rounded" style="position: absolute;margin-top:300px;margin-left:200px;background-color:darkgray;padding:8px;border-radius:10px;opacity:0.7;"><b><?php echo $row['product_name'] ?></b>
                            <br>
                            <b class="text-danger"><?php echo number_format($row['product_price']) . '$'; ?></b>
                        </div>
                        <img src="<?php echo '../admin/pages/' . $row['product_image'] ?>" width="100%" height="100%" alt="">

                    </div>
                </div>
            <?php endwhile; ?>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-multi" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-multi" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!-- featured categories -->
    <!-- featured categories -->
    <div class="small-container">
        <h2 class="tittle">ASUS</h2>
        <div class="row" id="asus">
            <?php $m = getByBrand('ASUS');
            $result = mysqli_query($con, $m);
            while ($row = $result->fetch_assoc()) :

            ?>
                <div class="col-4 mx-auto">
                    <img src="<?php echo "../admin/pages/" . $row['product_image'] ?>" alt="">
                    <h4 class="text-center"><?php echo $row['product_name'] ?></h4>
                    <div class="rating text-center">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <h3 class="cost text-center"><?php echo number_format($row['product_price']) . ' $'  ?></h3>
                </div>
            <?php endwhile; ?>
        </div>

        <!-- lastest products -->
        <h2 class="tittle">APPLE</h2>
        <div class="row" id="apple">
            <?php $m = getByBrand('APPLE');
            $result = mysqli_query($con, $m);
            while ($row = $result->fetch_assoc()) :

            ?>
                <div class="col-4">
                    <img src="<?php echo "../admin/pages/" . $row['product_image'] ?>" alt="">
                    <h4><?php echo $row['product_name'] ?></h4>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <h3 class="cost"><?php echo number_format($row['product_price']) . ' $'  ?></h3>
                </div>
            <?php endwhile; ?>
        </div>
        <h2 class="tittle">DELL</h2>
        <div class="row" id="dell">
            <?php $m = getByBrand('DELL');
            $result = mysqli_query($con, $m);
            while ($row = $result->fetch_assoc()) :

            ?>

                <div class="col-4">
                    <img src="<?php echo "../admin/pages/" . $row['product_image'] ?>" alt="">
                    <h4><?php echo $row['product_name'] ?></h4>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <h3 class="cost"><?php echo number_format($row['product_price']) . ' $'  ?></h3>
                </div>

            <?php endwhile; ?>
        </div>
        <h2 class="tittle">MSI</h2>
        <div class="row" id="msi">
            <?php $m = getByBrand('MSI');
            $result = mysqli_query($con, $m);
            while ($row = $result->fetch_assoc()) :

            ?>
                <div class="col-4">
                    <img src="<?php echo "../admin/pages/" . $row['product_image'] ?>" alt="">
                    <h4><?php echo $row['product_name'] ?></h4>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <h3 class="cost"><?php echo number_format($row['product_price']) . ' $'  ?></h3>
                </div>
            <?php endwhile; ?>
        </div>

    </div>
    <!-- offer -->
    <!-- Testimonial -->
    <!--  Brands -->
    <?php
    $sql = "SELECT * FROM brand";
    $query = mysqli_query($con, $sql);

    ?>
    <h2 class="tittle">Brand</h2>
    <div class="brands">
        <div class="small-container">
            <div class="row">
                <?php while ($row = $query->fetch_assoc()) : ?>
                    <div class="col-3">
                        <img src="../admin/pages/<?php echo $row['brand_image'] ?>" alt="">
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="footer-col-1">
                    <h3>Download Our App</h3>
                    <p>Download App for Android and Ios mobile phone</p>
                    <div class="app-logo">
                        <img src="./assets/images/play-store.png" alt="">
                        <img src="./assets/images/app-store.png" alt="">
                    </div>
                </div>
                <div class="footer-col-2">
                    <img src="https://images.cooltext.com/5539052.png" width="150" height="30" alt="MS COMPUTER" />
                    <p>Our Purpose Is To make the world fully of technology.
                    </p>
                </div>
                <div class="footer-col-3">
                    <h3>Useful Links</h3>
                    <ul>
                        <li>Coupons</li>
                        <li>Blog Post</li>
                        <li>Return Policy</li>
                        <li>Join Affiliate</li>
                    </ul>
                </div>
                <div class="footer-col-4">
                    <h3>Follow Us</h3>
                    <ul>
                        <li>Facebook</li>
                        <li>Twitter</li>
                        <li>Instagram</li>
                        <li>YouTube</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- js for toggle menu -->
    <script>
        //Allows bootstrap carousels to display 3 items per page rather than just one
        $(document).ready(function() {


            $('.carousel.carousel-multi .item').each(function() {
                var next = $(this).next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().attr("aria-hidden", "true").appendTo($(this));

                if (next.next().length > 0) {
                    next.next().children(':first-child').clone().attr("aria-hidden", "true").appendTo($(this));
                } else {
                    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
                }
                // if ($(this).hasClass('active')) {
                //     $('.carousel-caption').show();
                // } else {
                //     $('.carousel-caption').show(500);

                // }


            });
            $("#carousel-example-multi").carousel("cycle");

        });
    </script>


</body>

</html>