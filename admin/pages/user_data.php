<?php
require '../connection/config.php';

$queryData = "SELECT * FROM tbl_user";
$stetement = $con->query($queryData);
$total_row = mysqli_num_rows($stetement);
$table = '
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="10%">Image</th>
            <th width="15%">User Name</th>
            <th width="10%">User email Name</th>
            <th width="10%">User Password</th>
            <th width="20%">Action</th>
        </tr>
    </thead>
    ';
if ($total_row > 0) {
    $r = 1;
    while ($row = $stetement->fetch_assoc()) {
        $table .= '
    <tbody>
        <tr>
            <td>' . $r . '</td>
            <td>
                <img src="' . ($row['user_image']) . '" height="60" width="75" class="img-thumbnail" />
            </td>
            <td>' . $row["user_name"] . '</td>
            <td>' . $row["user_email"] . '</td>
            <td>' . $row["user_password"] . '</td>
            <td>
                <button type="button" data-id="' . $row["user_id"] . '" class="btn btn-warning btn-xs" id="on_edit"><i class="fa fa-edit fa-fw" aria-hidden="true"></i>Edit</button>
                    <button type="button" data-id="' . $row["user_id"] . '" class="btn btn-danger btn-xs" id="on_delete"><i class="fa fa-trash fa-fw" aria-hidden="true"></i>Delete</button>
                </td>
            </tr>
        </tbody>';
        $r++;
    }
} else {
    $table .= '<tr colspan="4" align="center">Data not found</tr>';
}
$table .= '</table>';
echo $table;
