<?php

require_once '../connection/config.php';
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password'])) {
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $password = password_hash(trim($_POST['password']), PASSWORD_DEFAULT);
    $stetement = "SELECT user_email FROM tbl_user WHERE user_email ='$email'";
    $executeStetement = mysqli_query($con, $stetement);
    $total_row = mysqli_num_rows($executeStetement);
    if ($total_row > 0) {
        echo json_encode(['status' => 'error', 'message' => 'sorry this email is ready taken']);
    } else {

        $image =  $_FILES['image'];
        $location = 'upload/user_avata/' . basename($_FILES['image']['name']);
        $extension = pathinfo($location, PATHINFO_EXTENSION);
        if ($extension != 'jpg' && $extension != 'jpag' && $extension != 'png') {
            echo json_encode(['messageError' => 'Only jpg, jpag and png are allowed']);
        } elseif ($_FILES['image']['size'] > 1000000) {
            echo json_encode(['messageError' => 'File is too big!']);
        } elseif (file_exists($location)) {
            echo json_encode(['messageError' => 'File is alreay uploaded!']);
        } elseif (move_uploaded_file($_FILES['image']['tmp_name'], $location) || $extension == '') {
            $query = "INSERT INTO tbl_user (user_name,user_email,user_password,user_image) VALUES ('$name','$email','$password','$location')";
            $insertData = mysqli_query($con, $query);
            $con->close();
            if ($insertData) {
                $_SESSION['create'] = "Your account has been created successfully";
                echo json_encode(['status' => 'success']);
            }
        }
    }
}

// login

if (isset($_POST['action']) == 'login') {
    $logEmail = $_POST['log_email'];
    $logPassword = $_POST['log_password'];
    $logName = $_POST['log_name'];
    $Query = "SELECT * FROM tbl_user WHERE user_email='$logEmail'";
    $getData = mysqli_query($con, $Query);
    if (mysqli_num_rows($getData) > 0) {
        $row = $getData->fetch_object();
        $userPassword = $row->user_password;
        $name = $row->user_name;
        $id = $row->user_id;
        if (password_verify($logPassword, $userPassword)) {
            $_SESSION['id'] = $id;
            $_SESSION['name'] = $name;
            echo json_encode(['status' => 'success']);
        } else {
            echo json_encode(['status' => 'errorPassword', 'message' => 'Your password is wrong']);
        }
    } else {
        echo json_encode(['status' => 'emailError', 'message' => 'Your email is Wrong']);
    }
}


//Edit User 

if (isset($_POST['get_data'])) {
    $getId = $_POST['id'];
    $selectData = "SELECT * FROM tbl_user WHERE user_id=$getId";
    $myData = mysqli_query($con, $selectData);
    $data = $myData->fetch_assoc();
    echo json_encode($data);
}


// Start Update

if (isset($_POST['up_id'])) {
    $old_password = $_POST['old_password'];
    $new_password = password_hash(trim($_POST['new_password']), PASSWORD_DEFAULT);
    $data_old_password = $_POST['data_old_password'];
    $up_user_name = $_POST['up_user_name'];
    $up_image = $_FILES['up_image'];
    $up_email = $_POST['up_email'];
    $old_image = $_POST['old_image'];

    $up_id = (int)$_POST['up_id'];
    $update_location = 'upload/user_avata/' . basename($_FILES['up_image']['name']);
    // echo var_dump($_FILES['up_image']);
    // exit;
    $extension = pathinfo($update_location, PATHINFO_EXTENSION);
    if (password_verify($old_password, $data_old_password)) {
        if ($extension != 'jpg' && $extension != 'jpag' && $extension != 'png') {
            echo 'Please select an image type jpg, png, jpag';
        } elseif ($_FILES['up_image']['size'] > 1000000) {
            echo 'File is too big!';
        } elseif (file_exists($update_location)) {
            echo 'File is alreay uploaded!';
        } elseif (move_uploaded_file($_FILES['up_image']['tmp_name'], $update_location)) {
            $update = "UPDATE tbl_user SET user_name='$up_user_name',user_email='$up_email',user_password='$new_password',user_image='$update_location' WHERE user_id=$up_id";
            $startUpdate = mysqli_query($con, $update);

            if ($startUpdate) {
                echo 'Success Update!';
                unlink($old_image);
            }
        }
    } else {
        echo 'Wrong password!!';
    }
}


// delete

if (isset($_POST['deleteData'])) {
    $getId = $_POST['deleteId'];
    $getData = "SELECT * FROM tbl_user WHERE user_id=$getId";
    $executeData = mysqli_query($con, $getData);
    $deleteData = $executeData->fetch_assoc();
    echo json_encode($deleteData);
}

if (isset($_POST['on_delete'])) {
    $idDelete = $_POST['idDelete'];
    $pathImage = $_POST['imageDelete'];
    $scriptDelete = "DELETE FROM tbl_user WHERE user_id=$idDelete";
    $executeDelete = mysqli_query($con, $scriptDelete);
    unlink($pathImage);
}
