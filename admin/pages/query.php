<?php
require_once '../connection/config.php';
//Get Data Product
if (isset($_POST['getProduct'])) {
    $query = "SELECT  products.id,products.product_image,products.product_name,products.screen_size,products.product_price,os.os_name,brand.brand_name,processor.processor_name,ram.ram_name,ram.ram_size,storage_type.storage_type_name,storage.storage_size,category.category_name from products 
LEFT JOIN brand ON brand.brand_id = products.brand_id 
LEFT JOIN processor ON processor.processor_id = products.processor_id 
LEFT JOIN ram ON ram.ram_id = products.ram_id 
LEFT JOIN os ON os.os_id = products.os_id 
LEFT JOIN category ON category.category_id = products.category_id 
LEFT JOIN storage ON storage.storage_id = products.storage_id 
LEFT JOIN storage_type ON storage_type.storage_type_id = storage.storage_type_id";

    if (isset($_POST["search"]["value"])) {
        $query .= '
 WHERE product_name LIKE "%' . $_POST["search"]["value"] . '%" 
 OR storage_size LIKE "%' . $_POST["search"]["value"] . '%" 
 OR ram_size LIKE "%' . $_POST["search"]["value"] . '%" 
 OR brand_name LIKE "%' . $_POST["search"]["value"] . '%" 
 OR category_name LIKE "%' . $_POST["search"]["value"] . '%" 
 OR product_price LIKE "%' . $_POST["search"]["value"] . '%" 
 ';
    }

    $query1 = '';

    if ($_POST["length"] != -1) {
        $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
    }

    $number_filter_row = mysqli_num_rows(mysqli_query($con, $query));

    $result = mysqli_query($con, $query . $query1);

    $data = array();
    $r = 1;
    while ($row = mysqli_fetch_array($result)) {

        $sub_array = array();
        $sub_array[] = '<td contenteditable><input data-id="' . $row["id"] . '" class="checkbox" type="checkbox"  id="select_delete"></td>';
        $sub_array[] = '<td contenteditable data-id="' . $row["id"] . '">' . $r++ . '</td>';

        $sub_array[] = '<td contenteditable>
                        <img src="' . ($row['product_image']) . '" height="60" width="75" class="img-thumbnail" />
                    </td>';
        $sub_array[] = '<td contenteditable>
                        ' . ($row['category_name']) . '
                    </td>';
        $sub_array[] = '<td contenteditable>
                        ' . ($row['brand_name']) . '
                    </td>';
        $sub_array[] = '<td contenteditable>
                        ' . ($row['product_name']) . '
                    </td>';
        $sub_array[] = '<td contenteditable>
                    ' . (number_format($row['product_price'])) . '$' . '
                </td>';
        $sub_array[] = '<td contenteditable>
                        ' . ($row['processor_name']) . '
                    </td>';
        $sub_array[] = '<td contenteditable>
                        ' . $row['storage_type_name'] . '-' . $row["storage_size"] . 'GB' .  '
                    </td>';
        $sub_array[] = '<td contenteditable>
                        ' . $row['ram_name'] . '-' . $row["ram_size"] . 'GB' .  '
                    </td>';
        $sub_array[] = '<td contenteditable>
                        ' . $row['screen_size'] . '
                    </td>';
        $sub_array[] = '<td contenteditable>
                    ' . $row['os_name'] . '
                </td>';
        $sub_array[] = '<td>
                        <button
                    type="button" id="on_view" class="btn btn-primary btn-xs" data-id="' . $row["id"] . '"><i class="fa fa-eye fa-fw" aria-hidden="true"></i>View
                    </button>
        </td>';
        $sub_array[] = '<td><button type="button" id="on_edit" class="btn btn-warning btn-xs" data-id="' . $row["id"] . '"><i class="fa fa-edit fa-fw" aria-hidden="true"></i>Edit</button></td>';
        $sub_array[] = '<td><button type="button" id="on_delete" class="btn btn-danger btn-xs" data-id="' . $row["id"] . '"><i class="fa fa-trash fa-fw" aria-hidden="true"></i>Delete</button></td>';
        $data[] = $sub_array;
    }

    function get_all_data($con)
    {
        $query = "SELECT  products.id,products.product_image,products.product_name,products.screen_size,products.product_price,os.os_name,brand.brand_name,processor.processor_name,ram.ram_name,ram.ram_size,storage_type.storage_type_name,storage.storage_size,category.category_name from products 
 LEFT JOIN brand ON brand.brand_id = products.brand_id 
 LEFT JOIN processor ON processor.processor_id = products.processor_id 
 LEFT JOIN ram ON ram.ram_id = products.ram_id 
 LEFT JOIN os ON os.os_id = products.os_id 
 LEFT JOIN category ON category.category_id = products.category_id 
 LEFT JOIN storage ON storage.storage_id = products.storage_id 
 LEFT JOIN storage_type ON storage_type.storage_type_id = storage.storage_type_id";
        $result = mysqli_query($con, $query);
        return mysqli_num_rows($result);
    }

    $output = array(
        "draw"    => intval($_POST["draw"]),
        "recordsTotal"  =>  get_all_data($con),
        "recordsFiltered" => $number_filter_row,
        "data"    => $data
    );

    echo json_encode($output);
}
