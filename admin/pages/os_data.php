<?php
require '../connection/config.php';

if (isset($_POST['get_data_type'])) {
    $queryData = "SELECT * FROM os";
    $stetement = $con->query($queryData);
    $total_row = mysqli_num_rows($stetement);
    $table = '
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="5%">os Name</th>
                <th width="20%">os Description</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        ';
    if ($total_row > 0) {
        $r = 1;
        while ($row = $stetement->fetch_assoc()) {
            $table .= '
        <tbody>
            <tr>
                <td>' . $r . '</td>
                <td>' . $row["os_name"] . '</td>
                <td>' . $row["os_description"] . '</td>
                <td>
                    <button type="button" data-id="' . $row["os_id"] . '" class="btn btn-warning btn-xs" id="on_edit"><i class="fa fa-edit fa-fw" aria-hidden="true"></i>Edit</button>
                    <button type="button" data-id="' . $row["os_id"] . '" class="btn btn-danger btn-xs" id="on_delete"><i class="fa fa-trash fa-fw" aria-hidden="true"></i>Delete</button>
                </td>
            </tr>
        </tbody>';
            $r++;
        }
    } else {
        $table .= '<tr class="alert alert-danger" colspan="4" align="center">Data not found</tr>';
    }
    $table .= '</table>';
    echo $table;
}


// insert

if (isset($_POST['insert_os'])) {
    $os_name = $_POST['os_name'];
    $os_description = $_POST['os_description'];
    $insert = "INSERT INTO os (os_name,os_description) VALUES ('$os_name','$os_description')";
    $os_result = mysqli_query($con, $insert);
    if ($os_result) {
        echo 'Data Save';
    } else {
        echo 'Fail to Add Type';
    }
}



//get data

if (isset($_POST['get_edit_type'])) {
    $id = $_POST['up_id'];
    $selectData = "SELECT * FROM os WHERE os_id=$id";
    $execute = mysqli_query($con, $selectData);
    $data = $execute->fetch_assoc();
    echo json_encode($data);
}
// update os

if (isset($_POST['up_os_name'])) {
    $up_os_name = $_POST['up_os_name'];
    $up_os_description = $_POST['up_os_description'];
    $up_id = $_POST['up_id'];

    $update = "UPDATE os SET os_name='$up_os_name',os_description='$up_os_description' WHERE os_id=$up_id";
    $result = mysqli_query($con, $update);
    if ($result) {
        echo 'Data Save';
    } else {
        echo 'Something went wrong';
    }
}


// delete storage type

if (isset($_POST['delete_id_type'])) {
    $get_id_delete = $_POST['delete_id_type'];
    $getData = "SELECT * FROM os WHERE os_id=$get_id_delete";
    $executeData = mysqli_query($con, $getData);
    $deleteData = $executeData->fetch_assoc();
    echo json_encode($deleteData);
}

if (isset($_POST['on_delete_os'])) {
    $id_delete_type = $_POST['id_delete_type'];
    $scriptDelete = "DELETE FROM os WHERE os_id=$id_delete_type";
    $executeDelete = mysqli_query($con, $scriptDelete);
}
