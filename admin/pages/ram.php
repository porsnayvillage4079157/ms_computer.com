<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Table RAM</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/startmin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>

    <div id="wrapper">
        <?php include '../include/side_bar.php'; ?>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Table RAM</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                List RAM
                                <div class="text-right">
                                    <button type="button" class="btn btn-md btn-primary" id="btn_ram"><i class="fa fa-plus fa-fw" aria-hidden="true"></i> Enter RAM</button>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive" id="ram_table">
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

    </div>

    <!-- Insert -->

    <div id="modal_insert_ram" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div style="position:relative;" id="parent_message">
                        <p style="position: absolute;right:0%;" class="text-right alert-danger" id="insert_message"></p>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Add ram</h4>
                </div>
                <div class="modal-body">
                    <!-- <div class="text-center">
                        <div id="image_display"></div>

                    </div> -->
                    <form method="POST" enctype="multipart/form-data" id="form_insert_ram" style="padding:0px 30px 0 30px ;" class="ml-auto">
                        <div class="form-group col-md-12">
                            <label for="productName">ram Name</label>
                            <input type="text" class="form-control" name="ram_name_insert" id="ram_name" placeholder=" ram">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="price">ram Description</label>
                            <input type="text" class="form-control" name="ram_description" id="ram_description" placeholder="ram Description">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="price">ram size</label>
                            <input type="text" class="form-control" name="ram_size" id="ram_size" placeholder="ram size">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="price">ram Image</label>
                            <input type="file" class="form-control" name="ram_image" id="ram_image autocomplete=" off">
                        </div>
                        <input type="submit" class="btn btn-primary" id="get_update" value="Edit">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <!-- ram  -->

    <div id="modal_update_ram" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div style="position:relative;" id="parent_message">
                        <p style="position: absolute;right:0%;" class="text-right alert-danger" id="update_message"></p>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Edit ram</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div id="image_display"></div>

                    </div>
                    <form method="POST" enctype="multipart/form-data" id="form_update_ram" style="padding:0px 30px 0 30px ;" class="ml-auto">
                        <div class="form-group col-md-12">
                            <label for="productName">ram Name</label>
                            <input type="text" class="form-control" name="up_ram_name" id="up_ram_name" placeholder=" ram">
                            <input type="hidden" name="up_id" id="up_id">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="price">ram Description</label>
                            <input type="text" class="form-control" name="up_ram_size" id="up_ram_size" placeholder="ram size">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="price">ram Description</label>
                            <input type="text" class="form-control" name="up_ram_description" id="up_ram_description" placeholder="ram Description">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="price">ram Image</label>
                            <input type="hidden" name="old_image" id="old_image">
                            <input type="file" class="form-control" name="up_image" id="up_ram_image autocomplete=" off">
                        </div>
                        <input type="submit" class="btn btn-primary" id="update" value="Edit">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h3 class="modal-title text-center" id="exampleModalLongTitle">Delete RAM</h3>
                    <div id="parent_message">
                        <p class="text-right alert-danger" id="delete_message"></p>
                    </div>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="delete_image" id="delete_image">
                    <input type="hidden" name="delete_id" id="delete_id">
                    <h4 id="show_delete"></h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_close" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn_delete">Delete</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../js/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../js/startmin.js"></script>
    <script src="../js/ajax/ram.js"></script>

</body>

</html>