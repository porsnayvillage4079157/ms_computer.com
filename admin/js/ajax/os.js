$(document).ready(function () {
    getData();
    addos();
    get_edit();
    on_delete();

});


function getData() {
    $.ajax({
        type: "post",
        url: "os_data.php",
        data: { get_data_type: 'get_data' },
        success: function (data) {
            $('#os_table').html(data);

        }
    });
}

// add Strage type

function addos() {
    $(document).on('click', '#btn_os', function (e) {
        $('#modal_insert_os').modal('show');
        $(document).on('submit', '#form_insert_os', function (e) {
            e.preventDefault();
            var osName = $('#os_name').val();
            var osDescription = $('#os_description').val();
            if (osName == '' || osDescription == '') {
                $('#insert_message').html('Please fiel in the blank').fadeIn().fadeOut(5000);
            } else if (osName.length < 2) {
                $('#insert_message').html('User Name Must least then 2 character').fadeIn().fadeOut(5000);
            } else {
                $.ajax({
                    method: 'POST',
                    url: "os_data.php",
                    data: {
                        insert_os: 'os',
                        os_name: osName,
                        os_description: osDescription,
                    },
                    success: function (response) {
                        $('#insert_message').html(response).fadeIn().fadeOut(4000);
                        $('#form_insert_os').trigger('reset');
                        getData();

                    }
                });
            }

        });


    });



}


//update


function get_edit() {
    $(document).on('click', '#on_edit', function () {
        var id = $(this).attr('data-id');
        console.log(id)
        $.ajax({
            type: "post",
            url: "os_data.php",
            data: {
                get_edit_type: 'edit',
                up_id: id,
            },
            dataType: "JSON",
            success: function (data) {
                $('#up_id').val(data.os_id);
                $('#up_os_name').val(data.os_name);
                $('#up_os_description').val(data.os_description);
                $('#modal_update_os').modal('show');
                console.log(data.storag_id)


            }
        });

    });

    $(document).on('submit', '#form_update_os', function (e) {
        e.preventDefault();
        var up_os_name = $('#up_os_name').val();
        var up_os_description = $('#up_os_description').val();
        var up_id = $('#up_id').val();

        if (up_os_name == '' || up_os_description == '') {

            $('#update_message').html(' id="update_message" class="alert-danger p-1">Please fiel in the blank').fadeIn().fadeOut(4000);

        } else if (up_os_name.length < 2) {

            $('#update_message').html(' id="update_message" class="alert-danger p-1">User Name Must least then 2 character').fadeIn().fadeOut(4000);

        } else {
            $.ajax({
                method: 'POST',
                url: "os_data.php",
                data: {
                    up_os_name: up_os_name,
                    up_os_description: up_os_description,
                    up_id: up_id,
                },
                success: function (response) {
                    $('#update_message').html(response).fadeIn().fadeOut(4000);
                    $(this).trigger('reset');
                    getData();

                }
            });
        }


    });

}


// delete os

function on_delete() {
    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');
        var getDeleteData = 'delete';
        $.ajax({
            url: "os_data.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                delete_data_type: getDeleteData,
                delete_id_type: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#delete_id').val(data.os_id);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.os_name + "</strong>?</h4>");
            }
        });

    });

    $('#btn_delete').click(function (e) {
        var idDelete = $('#delete_id').val();
        var on_delete = 'on_delete';
        $.ajax({
            method: 'POST',
            url: "os_data.php",
            data: {
                on_delete_os: on_delete,
                id_delete_type: idDelete,
            },
            success: function (response) {
                $('#delete_modal').modal('hide');
                getData();
            }
        });



    });
}