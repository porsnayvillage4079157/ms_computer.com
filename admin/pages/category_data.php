<?php
require '../connection/config.php';

if (isset($_POST['get_data_type'])) {
    $queryData = "SELECT * FROM category";
    $stetement = $con->query($queryData);
    $total_row = mysqli_num_rows($stetement);
    $table = '
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="5%">Category Name</th>
                <th width="20%">Category Description</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        ';
    if ($total_row > 0) {
        $r = 1;
        while ($row = $stetement->fetch_assoc()) {
            $table .= '
        <tbody>
            <tr>
                <td>' . $r . '</td>
                <td>' . $row["category_name"] . '</td>
                <td>' . $row["category_description"] . '</td>
                <td>
                    <button type="button" data-id="' . $row["category_id"] . '" class="btn btn-warning btn-xs" id="on_edit"><i class="fa fa-edit fa-fw"></i>Edit</button>
                    <button type="button" data-id="' . $row["category_id"] . '" class="btn btn-danger btn-xs" id="on_delete"><i class="fa fa-trash fa-fw"></i>Delete</button>
                </td>
            </tr>
        </tbody>';
            $r++;
        }
    } else {
        $table .= '<tr class="alert alert-danger" colspan="4" align="center">Data not found</tr>';
    }
    $table .= '</table>';
    echo $table;
}


// INsert

if (isset($_POST['insert_category'])) {
    $category_name = $_POST['category_name'];
    $category_description = $_POST['category_description'];
    $insert = "INSERT INTO category (category_name,category_description) VALUES ('$category_name','$category_description')";
    $category_result = mysqli_query($con, $insert);
    if ($category_result) {
        echo 'Data Save';
    } else {
        echo 'Fail to Add Type';
    }
}

// edit

if (isset($_POST['get_edit_type'])) {
    $id = $_POST['up_id'];
    $selectData = "SELECT * FROM category WHERE category_id=$id";
    $execute = mysqli_query($con, $selectData);
    $data = $execute->fetch_assoc();
    echo json_encode($data);
}
// update category

if (isset($_POST['up_category_name'])) {
    $up_category_name = $_POST['up_category_name'];
    $up_category_description = $_POST['up_category_description'];
    $up_id = $_POST['up_id'];

    $update = "UPDATE category SET category_name='$up_category_name',category_description='$up_category_description' WHERE category_id=$up_id";
    $result = mysqli_query($con, $update);
    if ($result) {
        echo 'Data Save';
    } else {
        echo 'Something went wrong';
    }
}


// DELETE

if (isset($_POST['delete_id_type'])) {
    $get_id_delete = $_POST['delete_id_type'];
    $getData = "SELECT * FROM category WHERE category_id=$get_id_delete";
    $executeData = mysqli_query($con, $getData);
    $deleteData = $executeData->fetch_assoc();
    echo json_encode($deleteData);
}

if (isset($_POST['on_delete_category'])) {
    $id_delete_type = $_POST['id_delete_type'];
    $scriptDelete = "DELETE FROM category WHERE category_id=$id_delete_type";
    $executeDelete = mysqli_query($con, $scriptDelete);
}
