<?php
require '../connection/config.php';

if (isset($_POST['get_data'])) {
    $queryData = "SELECT e.storage_id,u.storage_type_name, e.storage_size,e.storage_description, e.storage_image FROM storage_type AS u RIGHT JOIN storage AS e ON e.storage_type_id = u.storage_type_id";
    $stetement = $con->query($queryData);
    $total_row = mysqli_num_rows($stetement);
    $table = '
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="10%">Image</th>
                <th width="5%">Storage Name</th>
                <th width="5%">Storage Size</th>
                <th width="20%">Storage Description</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        ';
    if ($total_row > 0) {
        $r = 1;
        while ($row = $stetement->fetch_assoc()) {
            $table .= '
        <tbody>
            <tr>
                <td>' . $r . '</td>
                <td>
                   <img src="' . ($row['storage_image']) . '" height="60" width="75" class="img-thumbnail" />
                </td>
                <td>' . $row["storage_type_name"] . '</td>
                <td>' . $row["storage_size"] . '</td>
                <td>' . $row["storage_description"] . '</td>
                <td>
                    <button type="button" data-id="' . $row["storage_id"] . '" class="btn btn-warning btn-xs" id="on_edit"><i class="fa fa-edit fa-fw" aria-hidden="true"></i>Edit</button>
                    <button type="button" data-id="' . $row["storage_id"] . '" class="btn btn-danger btn-xs" id="on_delete"><i class="fa fa-trash fa-fw" aria-hidden="true"></i>Delete</button>
                </td>
            </tr>
        </tbody>';
            $r++;
        }
    } else {
        $table .= '<tr class="alert alert-danger" colspan="4" align="center">Data not found</tr>';
    }
    $table .= '</table>';
    echo $table;
}



if (isset($_POST['storage_name_insert'])) {
    $storage_name = $_POST['storage_name_insert'];
    $storage_size = $_POST['storage_size'];
    $storage_description = $_POST['storage_description'];
    $image =  $_FILES['storage_image'];
    $location = 'upload/storage/' . basename($_FILES['storage_image']['name']);
    $extension = pathinfo($location, PATHINFO_EXTENSION);
    if ($extension != 'jpg' && $extension != 'jpag' && $extension != 'png') {
        echo 'Only jpg, jpag and png are allowed';
    } elseif ($_FILES['storage_image']['size'] > 2000000) {
        echo 'File is too big!';
    } elseif (file_exists($location)) {
        echo 'File is alreay uploaded!';
    } elseif (move_uploaded_file($_FILES['storage_image']['tmp_name'], $location)) {
        $insert = "INSERT INTO storage (storage_type_id,storage_size,storage_description,storage_image) VALUES ($storage_name,'$storage_size','$storage_description','$location')";
        mysqli_query($con, $insert);
        echo 'Data Save!';
    } else {
        echo 'Sometiong went wrong!';
    }
}
//selectbox
if (isset($_POST['drowpdownData'])) {
    $scriptSelect = "SELECT * FROM storage_type";

    $data = mysqli_query($con, $scriptSelect);
    $total_row = mysqli_num_rows($data);
    $output = '<select class="form-control" name="storage_name">
    <option value="">---Select---</option>';
    if ($total_row > 0) {
        while ($row = $data->fetch_assoc()) {
            $output .= '<option value="' . $row['storage_type_id'] . '">' . $row['storage_type_name'] . '</option>';
        }
    } else {
        $output .= '<option>Data not found!</option>';
    }
    $output .= '</select>';
    echo json_encode($output);
}
//edit on storage

if (isset($_POST['get_edit'])) {
    $id = $_POST['up_id'];
    $selectData = "SELECT * FROM storage WHERE storage_id=$id";
    $execute = mysqli_query($con, $selectData);
    $data = $execute->fetch_assoc();
    echo json_encode($data);
}

// Update 

if (isset($_POST['up_storage_name'])) {
    $up_storage_name = $_POST['up_storage_name'];
    $up_storage_size = $_POST['up_storage_size'];
    $deleteImage = $_POST['old_image'];
    $up_storage_description = $_POST['up_storage_description'];
    $up_id = $_POST['up_id'];
    $locationUpdate = 'upload/storage/' . basename($_FILES['up_image']['name']);
    $imageUpdate =  $_FILES['up_image'];
    $extensionUpdate = pathinfo($locationUpdate, PATHINFO_EXTENSION);
    if ($extensionUpdate != 'jpg' && $extensionUpdate != 'jpag' && $extensionUpdate != 'png') {
        echo 'Only jpg, jpag and png are allowed';
    } elseif ($_FILES['up_image']['size'] > 2000000) {
        echo 'File is too big!';
    } elseif (file_exists($locationUpdate)) {
        echo 'File is alreay uploaded!';
    } elseif (move_uploaded_file($_FILES['up_image']['tmp_name'], $locationUpdate)) {
        $update = "UPDATE storage SET storage_type_id=$up_storage_name,storage_size='$up_storage_size',storage_description='$up_storage_description',storage_image='$locationUpdate' WHERE storage_id=$up_id";

        $result = mysqli_query($con, $update);
        if ($result) {
            unlink($deleteImage);
            echo 'Data save!';
        }
    } else {
        echo 'Sometiong went wrong!';
    }
}

// delete
if (isset($_POST['deleteData'])) {
    $getId = $_POST['deleteId'];
    // $getData = "SELECT * FROM storage WHERE storage_id='$getId'";
    $getData = "SELECT e.storage_id,u.storage_type_name, e.storage_size,e.storage_description, e.storage_image FROM storage_type AS u RIGHT JOIN storage AS e ON e.storage_type_id = u.storage_type_id WHERE storage_id=$getId";
    $executeData = mysqli_query($con, $getData);
    $deleteData = $executeData->fetch_assoc();
    echo json_encode($deleteData);
}

if (isset($_POST['on_delete'])) {
    $idDelete = $_POST['idDelete'];
    $pathImage = $_POST['imageDelete'];
    $scriptDelete = "DELETE FROM storage WHERE storage_id=$idDelete";
    $executeDelete = mysqli_query($con, $scriptDelete);
    unlink($pathImage);
}



// storage_type

if (isset($_POST['get_data_type'])) {
    $queryData = "SELECT * FROM storage_type";
    $stetement = $con->query($queryData);
    $total_row = mysqli_num_rows($stetement);
    $table = '
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="5%">Storage_type Name</th>
                <th width="20%">Storage_type Description</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        ';
    if ($total_row > 0) {
        $r = 1;
        while ($row = $stetement->fetch_assoc()) {
            $table .= '
        <tbody>
            <tr>
                <td>' . $r . '</td>
                <td>' . $row["storage_type_name"] . '</td>
                <td>' . $row["storage_type_description"] . '</td>
                <td>
                    <button type="button" data-id="' . $row["storage_type_id"] . '" class="btn btn-warning btn-xs" id="on_edit"><i class="fa fa-edit fa-fw"></i>Edit</button>
                    <button type="button" data-id="' . $row["storage_type_id"] . '" class="btn btn-danger btn-xs" id="on_delete"><i class="fa fa-trash fa-fw"></i>Delete</button>
                </td>
            </tr>
        </tbody>';
            $r++;
        }
    } else {
        $table .= '<tr class="alert alert-danger" colspan="4" align="center">Data not found</tr>';
    }
    $table .= '</table>';
    echo $table;
}


// insert

if (isset($_POST['insert_storage_type'])) {
    $storage_type_name = $_POST['storage_type_name'];
    $storage_type_description = $_POST['storage_type_description'];
    $insert = "INSERT INTO storage_type (storage_type_name,storage_type_description) VALUES ('$storage_type_name','$storage_type_description')";
    $storage_type_result = mysqli_query($con, $insert);
    if ($storage_type_result) {
        echo 'Data Save';
    } else {
        echo 'Fail to Add Type';
    }
}



//get data

if (isset($_POST['get_edit_type'])) {
    $id = $_POST['up_id'];
    $selectData = "SELECT * FROM storage_type WHERE storage_type_id=$id";
    $execute = mysqli_query($con, $selectData);
    $data = $execute->fetch_assoc();
    echo json_encode($data);
}
// update storage_type

if (isset($_POST['up_storage_type_name'])) {
    $up_storage_type_name = $_POST['up_storage_type_name'];
    $up_storage_type_description = $_POST['up_storage_type_description'];
    $up_id = $_POST['up_id'];

    $update = "UPDATE storage_type SET storage_type_name='$up_storage_type_name',storage_type_description='$up_storage_type_description' WHERE storage_type_id=$up_id";
    $result = mysqli_query($con, $update);
    if ($result) {
        echo 'Data Save';
    } else {
        echo 'Something went wrong';
    }
}


// delete storage type

if (isset($_POST['delete_id_type'])) {
    $get_id_delete = $_POST['delete_id_type'];
    $getData = "SELECT * FROM storage_type WHERE storage_type_id=$get_id_delete";
    $executeData = mysqli_query($con, $getData);
    $deleteData = $executeData->fetch_assoc();
    echo json_encode($deleteData);
}

if (isset($_POST['on_delete_storage_type'])) {
    $id_delete_type = $_POST['id_delete_type'];
    $scriptDelete = "DELETE FROM storage_type WHERE storage_type_id=$id_delete_type";
    $executeDelete = mysqli_query($con, $scriptDelete);
}
