$(document).ready(function () {
    getData();
    addBrand();
    get_edit();
    on_delete();

});

function getData() {
    $.ajax({
        url: 'brand_data.php',
        method: 'post',
        data: { get_brand_data: 'get_brand' },
        success: function (data) {
            $('#brand_table').html(data);
        }
    });

}

// add brand

function addBrand() {
    $(document).on('click', '#btn_brand', function (e) {
        $('#modal_insert_brand').modal('show');
        $(document).on('submit', '#form_insert_brand', function (e) {
            e.preventDefault();
            var brandName = $('#brand_name').val();
            var brandImage = $('#brand_image').val();
            if (brandName == '' || brandImage == '') {
                $('#insert_message').html('Please fiel in the blank').fadeIn().fadeOut(5000);
            } else if (brandName.length < 2) {
                $('#insert_message').html('User Name Must least then 2 character').fadeIn().fadeOut(5000);
            } else {
                $.ajax({
                    method: 'POST',
                    url: "brand_data.php",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('#insert_message').html(response);
                        $('#form_insert_brand').trigger('reset');
                        getData();

                    }
                });
            }

        });


    });



}

// GET EDIT BRAND

function get_edit() {
    $(document).on('click', '#on_edit', function () {
        var id = $(this).attr('data-id');
        console.log(id)
        $.ajax({
            type: "post",
            url: "brand_data.php",
            data: {
                get_edit: 'edit',
                up_id: id,
            },
            dataType: "JSON",
            success: function (data) {
                $('#up_id').val(data.brand_id);
                $('#up_brand_name').val(data.brand_name);
                $('#up_brand_description').val(data.brand_description);
                $('#image_display').html('<img  id="myImage" src="' + data.brand_image + '" alt="" width="150px" height="150px">');
                $('#old_image').val(data.brand_image);
                $('#modal_update_brand').modal('show');


            }
        });

    });

    $(document).on('submit', '#form_update_brand', function (e) {
        e.preventDefault();
        var up_brand_name = $('#up_brand_name').val();
        var up_brand_description = $('#up_brand_description').val();

        if (up_brand_name == '') {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">Please fiel in the blank</p>');

        } else if (up_brand_name.length < 2) {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">User Name Must least then 2 character</p>');

        } else {
            $.ajax({
                method: 'POST',
                url: "brand_data.php",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#update_message').html(response);
                    $(this).trigger('reset');
                    getData();

                }
            });
        }


    });

}

// On DELETE

function on_delete() {
    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');
        var deleteData = 'delete';
        $.ajax({
            url: "brand_data.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                deleteData: deleteData,
                deleteId: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#delete_id').val(data.brand_id);
                $('#delete_image').val(data.brand_image);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.brand_name + "</strong>?</h4>");
            }
        });

    });

    $('#btn_delete').click(function (e) {
        var idDelete = $('#delete_id').val();
        var imageDelete = $('#delete_image').val();
        console.log(imageDelete)
        var on_delete = 'on_delete';
        $.ajax({
            method: 'POST',
            url: "brand_data.php",
            data: {
                on_delete: on_delete,
                idDelete: idDelete,
                imageDelete: imageDelete,
            },
            success: function (response) {
                $('#delete_modal').modal('hide');
                getData();
            }
        });



    });
}


