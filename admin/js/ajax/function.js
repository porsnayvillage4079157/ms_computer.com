$(document).ready(function () {
    preventPress();
    on_update();
    getData();
    insert_form();
    on_delete();
    getDataDeleteAll();
    deleteAll();
    fetch_data();
    selectAll();
    viewProduct();
    manageColor();
});
// get Data to table
function fetch_data() {
    var dataTable = $('#user_data').DataTable({
        "processing": true,
        "serverSide": true,
        "order": false,
        "dom": 'Blfrtip',
        "ajax": {
            url: "query.php",
            type: "POST",
            data: { getProduct: 'get-product' },
            beforeSend: function () {
                $('#loading').html('<i class="fa fa-spinner fa-pulse fa-3x" aria-hidden="true"></i>');

            },
            complete: function () {
                $('#loading').html('');
            },
        }


    });
}

function data(category, brand, ram, processor, storage, os) {

    $.ajax({
        url: "dropdown_data.php",
        method: 'POST',
        data: { category: 'category' },
        dataType: 'JSON',
        success: function (response) {
            $('#' + category).html(response);


        }

    });


    $.ajax({
        url: "dropdown_data.php",
        method: 'POST',
        data: { os: 'os' },
        dataType: 'JSON',
        success: function (response) {
            $('#' + os).html(response);


        }

    });


    $.ajax({
        url: "dropdown_data.php",
        method: 'POST',
        data: { brand: 'brand' },
        dataType: 'JSON',
        success: function (response) {
            $('#' + brand).html(response);


        }

    });


    $.ajax({
        url: "dropdown_data.php",
        method: 'POST',
        data: { ram: 'get_ram' },
        dataType: 'JSON',
        success: function (response) {
            $('#' + ram).html(response);


        }

    });

    $.ajax({
        url: "dropdown_data.php",
        method: 'POST',
        data: { processor: 'get_ram' },
        dataType: 'JSON',
        success: function (response) {
            $('#' + processor).html(response);


        }

    });

    $.ajax({
        url: "dropdown_data.php",
        method: 'POST',
        data: { storage: 'get_storage' },
        dataType: 'JSON',
        success: function (response) {
            $('#' + storage).html(response);

        }

    });

}
// Insert Data

function getData() {
    $(document).on('click', '#insert_my_product', function () {
        $('#insert_modal').modal('show');
        data('category', 'brand', 'ram', 'processor', 'storage', 'os');
    });

}

function insert_form() {
    var arr = [];
    $(document).on('keydown', '#item', function (e) {
        if (e.which == 13) {
            var value = $('#item').val();
            var colorValue = $('.small').append('<div style="background:' + value + ';margin-left:10px;padding:4px !important;" class="alert text-dark text-center" role="alert"><strong>' + value + '!</strong> <a href="#" class="alert-link"></a></div>');
            // color.push(value);

            $("input[name='item[]']").each(function () {
                arr.push($(this).val());
                $('#color').val(arr);
            });
            console.log($('#color').val())
        }


    });
    $(document).on('submit', '#form_insert', function (e) {
        e.preventDefault();
        var category = $('#category').val();
        var brand = $('#brand').val();
        var productName = $('#productName').val();
        var processor = $('#processor').val();
        var ram = $('#ram').val();
        var hdd = $('#hdd').val();
        var screenSize = $('#screenSize').val();
        var os = $('#os').val();
        var price = $('#price').val();
        var colors = $('#color').val();
        var newColor = colors.split(',');

        if (brand == '' || productName == '' || processor == '' || ram == '' || hdd == '' || screenSize == '' || os == '' || price == '' || category == '') {
            $('#insert_message').html('Please fiel in the blank').fadeIn().fadeOut(5000);
        } else if (newColor.length < 3 || newColor.length > 3) {
            $('#insert_message').html('Product color must more than 2 or equal 3 colors').fadeIn().fadeOut(5000);
        } else {
            $.ajax({
                method: 'POST',
                url: "action.php",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    arr = [];
                    $('.small').html('');
                    $('#insert_message').html(response).fadeIn().fadeOut(4000);
                    $('#form_insert').trigger('reset');
                    $('#color').val('');
                    $('#user_data').DataTable().destroy();
                    fetch_data();

                }
            });
        }

    });
}





//   UPDATE function

function on_update() {
    data('up_category', 'up_brand', 'up_ram', 'up_processor', 'up_storage', 'up_os');
    changeColor();
    var arr = [];
    var colors = [];
    $(document).on('keydown', '#items', function (e) {
        if (e.which == 13) {
            $('.smalls').html('');
            var value = $('#items').val();
            var colorValue = $('.smallss').append('<div style="background:' + value + ';margin-left:10px;padding:4px !important;" class="alert text-dark text-center" role="alert"><strong><small>' + value + '!</small></strong> <a href="#" class="alert-link"></a></div>');

            $("input[name='items[]']").each(function () {
                colors.push($(this).val());
                $('#colors').val(colors);
            });
            console.log($('#colors'))
        }


    });
    function changeColor() {
        $(document).on('change', '#item1s', function () {
            $('#items').val($(this).val());

        });
    }
    $(document).on('click', '#on_edit', function () {

        var id = $(this).attr('data-id');
        var edit = 'edit';
        $.ajax({
            url: "action.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                edit: edit,
                id: id,
            },
            success: function (data) {
                colors = [];
                $('#modal_update').modal('show');
                $('#up_brand').val(data[0].brand_id);
                $('#up_productName').val(data[0].product_name);
                $('#up_processor').val(data[0].processor_id);
                $('#up_ram').val(data[0].ram_id);
                $('#up_storage').val(data[0].storage_id);
                $('#up_price').val(data[0].product_price);
                $('#up_screenSize').val(data[0].screen_size);
                $('#up_os').val(data[0].os_id);
                $('#up_id').val(data[0].id);
                $('#up_category').val(data[0].category_id);
                $('#image_delete').val(data[0].product_image);
                $('#image_display').html('<img id="myImage" src="' + data[0].product_image + '" alt="" width="100%" height="440px">');
                arr = data[1];
                $('#colors').val(arr);
                console.log($('#colors'));
                for (var i = 0; i < data[1].length; i++) {
                    $('.smalls').append('<div style="background:' + data[1][i] + ';width:50px;height:50px;margin-left:15px;border-radius:50%" class="colors-value alert alert-dismissible" role="alert"></div>');
                }
                $('.btn').click(function (e) {
                    $('.smalls,.smallss').html('');
                });

            }
        });

    });
    $(document).on('submit', '#form_update', function (e) {
        e.preventDefault();
        var up_brand = $('#up_brand').val();
        var up_productName = $('#up_productName').val();
        var up_processor = $('#up_processor').val();
        var up_ram = $('#up_ram').val();
        var up_storage = $('#up_storage').val();
        var up_price = $('#up_price').val();
        var up_screenSize = $('#up_screenSize').val();
        var up_os = $('#up_os').val();
        var colors = $('#colors').val();
        var newColor = colors.split(',');

        if (up_brand == '' || up_productName == '' || up_processor == '' || up_ram == '' || up_storage == '' || up_screenSize == '' || up_os == '' || up_price == '') {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">Please fiel in the blank</p>').fadeIn().fadeOut(4000);

        } else if (newColor.length < 3 || newColor.length > 3) {
            $('#update_message').html('<p id="update_message" class="alert-danger p-1">Product color must more than 2 or equal 3 colors</p>').fadeIn().fadeOut(4000);

        } else {
            $.ajax({
                method: 'POST',
                url: "action.php",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#update_message').html(response).fadeIn().fadeOut(4000);
                    $('.smallss').html('');
                    $('#user_data').DataTable().destroy();
                    fetch_data();

                }
            });
        }


    });
}

// Get Data To Delete


// DELECT Data

function on_delete() {

    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');
        var deleteData = 'delete';
        $.ajax({
            url: "action.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                deleteData: deleteData,
                deleteId: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#id').val(data.id);
                $('#delete_image').val(data.product_image);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.product_name + "</strong>?</h4>");
            }
        });

    });

    $('#btn_delete').click(function (e) {
        var idDelete = $('#id').val();
        var imageDelete = $('#delete_image').val();
        console.log(imageDelete)
        var on_delete = 'on_delete';
        $.ajax({
            method: 'POST',
            url: "action.php",
            data: {
                on_delete: on_delete,
                idDelete: idDelete,
                imageDelete: imageDelete,
            },
            success: function (response) {
                $('#delete_modal').modal('hide');
                $('#user_data').DataTable().destroy();
                fetch_data();
            }
        });



    });
}

// Select Data to Delete
function selectDelete(text_id) {
    var filterData = [];
    $('.' + text_id + ':checked').each(function () {
        filterData.push($(this).attr('data-id'));

    });
    return filterData;
}

function selectAll() {
    $(document).on('click', '#btn_select_all', function () {
        $('.checkbox').trigger('click');
    });
}
//Get Data to Delete

function getDataDeleteAll() {
    $(document).on('click', '.checkbox', function () {
        $('#deleteAll').click(function (e) {
            var deleteAllId = selectDelete('checkbox');
            var dataDeleteAll = 'data_delete_all';
            console.log(deleteAllId)
            $.ajax({
                url: "action.php",
                method: 'post',
                dataType: 'JSON',
                data: {
                    dataDeleteAll: dataDeleteAll,
                    deleteAllId: deleteAllId,
                },
                success: function (data) {
                    $('#show_delete_all').html('Are you sure want to delete <strong>' + selectDelete('checkbox').length + ' </strong> products?');
                    $('#delete_all_modal').modal('show');
                    $('#id_delete_all').val(data);
                }
            });
        });

    });
}

//Start Delete

function deleteAll() {
    $('#btn_delete_all').click(function (e) {
        var deleteIdAll = $('#id_delete_all').val();
        var startingDelete = 'starting_delete';
        $.ajax({
            method: 'POST',
            url: "action.php",
            data: {
                startingDelete: startingDelete,
                deleteIdAll: deleteIdAll,
            },
            success: function (response) {
                $('#delete_all_message').html(response);
                $('#delete_all_modal').modal('hide');
                $('#user_data').DataTable().destroy();
                fetch_data();
            }
        });

    });
}

function preventPress() {
    $(document).on('keypress', 'body', function (e) {
        if (e.which == 13) {
            e.preventDefault();
        }

    });
}
// View Product

function viewProduct() {
    $(document).on('click', '#on_view', function () {
        var idView = $(this).attr('data-id');
        var view = 'view';
        $.ajax({
            url: "action.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                view: view,
                idView: idView,
            },
            success: function (data) {
                $('#view_modal').modal('show');
                console.log(data)
                $('#modal_view').modal('show');
                $('#view_category').html(data[0].category_name);
                $('#view_brand').html(data[0].brand_name);
                $('#view_product_name').html(data[0].product_name);
                $('#view_processor').html(data[0].processor_name);
                $('#view_ram_name').html(data[0].ram_name);
                $('#view_ram_size').html(data[0].ram_size + 'GB');
                $('#view_storage').html(data[0].storage_type_name + '-' + data[0].storage_size + 'GB');
                $('#view_price').html(parseFloat(data[0].product_price).toFixed(2) + '$');
                $('#view_screen_size').html(data[0].screen_size + ' inch');
                $('#view_os').html(data[0].os_name);
                $('#image_delete').html(data[0].product_image);
                $('#product_name').html(data[0].product_name);
                $('#created_by').html(data[0].user_name);
                for (var i = 0; i < data[1].length; i++) {
                    $('.color-item').append('<li class="text-center" style="width:60px;height:60px;display:flex;align-items: center;margin-left:15px; border-radius: 50%;background:' + data[1][i] + '"><small class="text-center">' + data[1][i] + '</small></li>');
                }
                $('body').click(function () {
                    $('.color-item').html('')
                })
                $('#view_img').html('<img id="myImage" src="' + data[0].product_image + '" alt="" width="100%" height="440px">');

            }

        });
    });
}

function manageColor() {
    changeColor();

    function changeColor() {
        $(document).on('change', '#item1', function () {
            $('#item').val($(this).val());

        });
    }

    // $('form').submit(function (e) {
    //     e.preventDefault();

    // });

    // function submitColor() {
    //     $('#item').val();
    // }
}




