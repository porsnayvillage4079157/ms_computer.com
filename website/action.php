<?php
include 'config/config.php';
include 'config/function/index.php';

if (isset($_POST['action'])) {
    $sql = " SELECT  products.id,products.product_color,products.product_image,products.product_name,products.screen_size,products.product_price,os.os_name,brand.brand_name,processor.processor_name,ram.ram_name,ram.ram_size,storage_type.storage_type_name,storage.storage_size,category.category_name from products 
    LEFT JOIN brand ON brand.brand_id = products.brand_id 
    LEFT JOIN processor ON processor.processor_id = products.processor_id 
    LEFT JOIN ram ON ram.ram_id = products.ram_id 
    LEFT JOIN os ON os.os_id = products.os_id 
    LEFT JOIN category ON category.category_id = products.category_id 
    LEFT JOIN storage ON storage.storage_id = products.storage_id 
    LEFT JOIN storage_type ON storage_type.storage_type_id = storage.storage_type_id WHERE brand_name !=''";
    if (isset($_POST['brand'])) {
        $brand = implode("','", $_POST['brand']);
        $sql .= "AND brand_name IN('" . $brand . "')";
    }
    if (isset($_POST['ram'])) {
        $ram = implode("','", $_POST['ram']);
        $sql .= "AND ram_size IN('" . $ram . "')";
    }
    if (isset($_POST['hdd'])) {
        $hdd = implode("','", $_POST['hdd']);
        $sql .= "AND storage_size IN('" . $hdd . "')";
    }
    if (isset($_POST['processor'])) {
        $processor = implode("','", $_POST['processor']);
        $sql .= "AND processor_name IN('" . $processor . "')";
    }
    if (isset($_POST['screen_size'])) {
        $screen_size = implode("','", $_POST['screen_size']);
        $sql .= "AND screen_size IN('" . $screen_size . "')";
    }
    if (isset($_POST['os'])) {
        $os = implode("','", $_POST['os']);
        $sql .= "AND os_name IN('" . $os . "')";
    }
    $result = $con->query($sql);
    $output = '';

    if ($result->num_rows > 0) {
        $count = 0;
        while ($row = $result->fetch_assoc()) {
            $colors = unserialize($row['product_color']);
            $new_color = implode("", $colors);
            $real_color = explode(",", $new_color);
            $output .= '<div class="col-md-6 col-sm-12 mb-2 col-lg-4 mt-2 ex">
            <div class="card-group" id="card">
                <div class="card border-primary bg-dark text-light">
                    <img width="200px" height="250px" class="card-img-top" src="../admin/pages/' . $row['product_image'] . '">
                    <div class="card-body">
                        <h5 class="card-title bg-info rounded p-1">' . $row['product_name'] . '</h5>
                        <p class="card-text">
                        <h4 class="text-danger">Price: ' . number_format($row['product_price']) . '$' . '</h4>
                        Brand : ' . $row['brand_name'] . '  <br>
                                    RAM Name :' . $row['ram_name'] . ' <br>
                                    RAM Size : ' . $row['ram_size'] . '  GB  <br>
                                    Hard Disk : ' . $row['storage_type_name'] . ' -  ' . $row['storage_size'] . ' GB <br>
                                    Processor : ' . $row['processor_name'] . ' <br>
                                    Screen Size : ' . $row['screen_size'] . ' <br>
                                    Operation System : ' . $row['os_name'] . ' <br>
                        </p>
                    </div>
                    <div class="card-footer ">
                        <small class="text-center" style="display: flex;justify-content:center;">
                       <div class="color" style=" background:' . $real_color[0] . ';"></div>
                       <div class="color" style=" background:' . $real_color[1]  . ';"></div>
                       <div class="color" style=" background:' . $real_color[2] . ';"></div>
                       
                        </small>
                    </div>

                </div>



            </div>

        </div>';
            $count++;
        }
    } else {
        $output = '<p class="mx-auto text-danger">No Products Found</p>';
    }
    echo $output;
}
echo '<script type="text/javascript" src="assets/js/vanilla-tilt.js"></script>
                <script type="text/javascript" src="assets/js/script.js"></script>
                <link rel="stylesheet" href="assets/css/style.css">';
