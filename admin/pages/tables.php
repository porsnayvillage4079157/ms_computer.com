<?php require '../connection/config.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Table Products</title>

    <!-- Style -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../css/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="../css/startmin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        #user_data_wrapper .dt-buttons {
            display: flex;
            margin-left: 40%;
            top: -60px;
            position: absolute;
            width: 400px;
            justify-content: center;
        }

        #user_data_wrapper .dt-buttons button {
            margin-left: 10px;
            padding: 5px;
            background: #45f06d;
            border: none;
            box-shadow: none;
            border-radius: 4px;

        }

        #user_data_wrapper .dt-buttons button:hover {
            background: #ff0076;
        }

        .modal ul li {
            margin-bottom: 14px;
            font-size: 16px;
        }
    </style>
</head>

<body>
    <div id="wrapper">
        <?php include_once '../include/side_bar.php'; ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tables</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Products Table
                                <div class="text-right">
                                    <button id="btn_select_all" class="btn btn-warning"><i class="fa fa-asterisk" aria-hidden="true"></i> Select All</button>
                                    <button type="button" class="btn btn-danger btn-md fixed deleteAll" id="deleteAll"><i class="fa fa-trash fa-fw"></i> Delete All</button>
                                    <button type="button" id="insert_my_product" class="btn btn-md btn-primary"><i class="fa fa-plus fa-fw" aria-hidden="true"></i> Enter Product</button>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <table id="user_data" class="table table-bordered table-striped table-hover" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:2% !important">Select</th>
                                            <th class="text-center" style="width:2%">No</th>
                                            <th class="text-center" style="width:8%">Image</th>
                                            <th class="text-center" style="width:5%">Category</th>
                                            <th class="text-center" style="width:5%">Brand</th>
                                            <th class="text-center" style="width:24%">Product Name</th>
                                            <th class="text-center" style="width:5%">Price</th>
                                            <th class="text-center" style="width:7%">Processor</th>
                                            <th class="text-center" style="width:10%">HDD</th>
                                            <th class="text-center" style="width:16%">RAM</th>
                                            <th class="text-center" style="width:5%">Screen-Size</th>
                                            <th class="text-center" style="width:5%">OS</th>
                                            <th class="text-center" style="width:2%">Action</th>
                                            <th class="text-center" style="width:2%">Action</th>
                                            <th class="text-center" style="width:2%">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                <!-- /.table-responsive -->
                            </div>
                            <div class="text-center" id="loading"></div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>


    <!-- /#wrapper -->
    <!-- ------------------EDIT----------------------- -->

    <div class="modal fade" id="modal_update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-center" id="lognId">Edit Product</h3>
                    <div style="position:relative;" id="parent_message">
                        <p style="position: absolute;right:0%;" class="text-right alert-danger" id="update_message"></p>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="text-center">
                                <div id="image_display"></div>

                            </div>
                        </div>
                        <div class="col-md-7">
                            <form method="POST" enctype="multipart/form-data" id="form_update">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="category">Category</label>
                                        <select name="up_category" id="up_category" class="form-control"></select>

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="brand">Brand</label>
                                        <input type="hidden" name="up_id" id="up_id">
                                        <select class="form-control" id="up_brand" name="up_brand"></select>
                                    </div>

                                </div>
                                <div class="form-row">

                                    <div class="form-group col-md-6">
                                        <label for="productName">Product Name</label>
                                        <input type="text" class="form-control" name="up_product_name" id="up_productName" placeholder="Product Name">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="price">Price</label>
                                        <input type="text" class="form-control" name="up_price" id="up_price" placeholder="Price">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="screen_size">Screen Size</label>
                                        <input type="text" class="form-control" name="up_screen_size" id="up_screenSize" placeholder="Screen Size">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ram">RAM</label>
                                        <select class="form-control" name="up_ram" id="up_ram"></select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="screen_size">OS</label>
                                        <select class="form-control" name="up_os" id="up_os"></select>
                                        <input type="hidden" name="image_delete" id="image_delete">
                                    </div>

                                </div>


                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="RAM">Processor</label>
                                        <select name="up_processor" id="up_processor" class="form-control"></select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputZip">HDD</label>
                                        <select class="form-control" name="up_storage" id="up_storage"></select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <label for="inputZip">Image</label>
                                        <input type="file" class="form-control" name="up_image" id="up_image">
                                        <input type="hidden" name="up_user_id" value="<?php echo $_SESSION['id']; ?>">
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="inputZip">Color</label>
                                        <input type="text" class="form-control" name="items[]" id="items">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <input style="width: 50px;height:50px;border-radius:50%;margin-top:15px;" type="color" class="form-control" name="item1s" id="item1s">


                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-5">

                                        </div>
                                        <div class="form-group col-md-7">
                                            <div class="form-row">
                                                <input type="hidden" name="colors[]" id="colors">
                                                <div class="smalls col-md-2" style="display: flex;"></div>
                                                <div class="smallss col-md-2" style="display: flex;"></div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-primary" id="btnUpdate" value="Edit">
                            </form>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_close" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <!-- DELECT MODAL -->

    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h3 class="modal-title text-center" id="exampleModalLongTitle">Delete Product</h3>
                    <div id="parent_message">
                        <p class="text-right alert-danger" id="delete_message"></p>
                    </div>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="image" id="delete_image">
                    <input type="hidden" name="id" id="id">
                    <h4 id="show_delete"></h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_close" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn_delete">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Get Data Delete All -->

    <div class="modal fade" id="delete_all_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h3 class="modal-title text-center">Delete Product</h3>
                    <div id="parent_message">
                        <p class="text-right" id="delete_all_message"></p>
                    </div>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="" id="id_delete_all">
                    <h4 class="text-center" id="show_delete_all">Are you sure want to delete all records?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_close" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn_delete_all">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <!-- VIEW PRODUCT -->

    <div class="modal fade" id="view_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h3 class="modal-title text-center">
                        <p id="product_name"></p>
                    </h3>
                    <div id="parent_message">
                        <p class="text-right" id="view_message"></p>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="p-0 m-0" id="view_img"></div>
                            <div class="colors">
                                <ul class="color-item" style="display: flex;list-style:none;">

                                </ul>

                            </div>

                        </div>

                        <div class="col-md-6 text-left">
                            <form action="">
                                <div class="form-row">
                                    <ul style="list-style:square;display:inline">
                                        <li>
                                            Category: <strong id="view_category"></strong>
                                        </li>
                                        <li>
                                            Brand: <strong id="view_brand"></strong>
                                        </li>

                                        <li>
                                            Product Name: <strong id="view_product_name"></strong>
                                        </li>

                                        <li>
                                            Price: <strong id="view_price"></strong>
                                        </li>



                                        <li>
                                            Processor: <strong id="view_processor"></strong>
                                        </li>

                                        <li>
                                            RAM Name: <strong id="view_ram_name"></strong>
                                        </li>

                                        <li>
                                            RAM Size: <strong id="view_ram_size"></strong>
                                        </li>

                                        <li>
                                            Storage: <strong id="view_storage"></strong>
                                        </li>

                                        <li>
                                            Operation System: <strong id="view_os"></strong>
                                        </li>
                                        <li>
                                            Screen Size: <strong id="view_screen_size"></strong>
                                        </li>
                                        <li>
                                            Created By: <strong id="created_by"></strong>
                                        </li>
                                    </ul>

                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- INSERT -->


    <div class="modal fade" id="insert_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-center">Enter Product</h3>
                    <div id="parent_message" style="position: relative;">
                        <p style="position: absolute;right:0%;" class="text-right alert-danger" id="insert_message"></p>
                    </div>
                </div>
                <div class="modal-body">
                    <form method="POST" enctype="multipart/form-data" id="form_insert">
                        <div class="form-row col-md-6">
                            <div class="form-group ">
                                <input type="hidden" name="insert_product_to_db">
                                <label for="category">Category</label>
                                <select name="category" id="category" class="form-control"></select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="brand">Brand</label>
                            <select name="brand" id="brand" class="form-control"></select>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="productName">Product Name</label>
                                <input type="text" class="form-control" name="product_name" id="productName" placeholder="Product Name">
                                <input type="hidden" name="admin_id" value="<?php echo $_SESSION['id']; ?>" id="admin_id">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="price">Price</label>
                                <input type="text" class="form-control" name="price" id="price" placeholder="Price">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="screen_size">Screen Size</label>
                                <input type="text" class="form-control" name="screen_size" id="screenSize" placeholder="Screen Size">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ram">RAM</label>
                                <select name="ram" id="ram" class="form-control"></select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="screen_size">OS</label>
                                <select class="form-control" name="os" id="os"></select>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="RAM">Processor</label>
                                    <select name="processor" id="processor" class="form-control"></select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputZip">HDD</label>
                                    <select name="storage" id="storage" class="form-control"></select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label for="inputZip">image</label>
                                    <input type="file" class="form-control" name="image" id="file">

                                </div>
                                <div class="form-group col-md-5">
                                    <label for="inputZip">Color</label>
                                    <input type="text" class="form-control" name="item[]" id="item">
                                </div>
                                <div class="form-group col-md-2">
                                    <input style="width: 50px;height:50px;border-radius:50%;margin-top:15px;" type="color" class="form-control" name="item1" id="item1">


                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-5">

                                    </div>
                                    <div class="form-group col-md-7">
                                        <div class="form-row">
                                            <input type="hidden" name="color[]" id="color">
                                            <div class="small col-md-2" style="display: flex;"></div>

                                        </div>


                                    </div>
                                </div>


                            </div>
                            <input type="submit" class="btn btn-primary" value="Save" id="btn_submit">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_close" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
    <!-- jQuery -->
    <script src="../js/jquery.min.js"></script>
    <!-- AJAX -->


    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../js/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>

    <!-- export -->
    <!-- <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>











    <!-- <script src="../js/dataTables/script.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="../js/startmin.js"></script>
    <script src="../js/ajax/function.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
</body>

</html>