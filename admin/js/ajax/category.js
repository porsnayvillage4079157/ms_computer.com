$(document).ready(function () {
    getData();
    addCategory();
    get_edit();
    on_delete();


});


function getData() {
    $.ajax({
        type: "post",
        url: "category_data.php",
        data: { get_data_type: 'get_data' },
        success: function (data) {
            $('#category_table').html(data);

        }
    });
}

// INSERT DATA

function addCategory() {
    $(document).on('click', '#btn_category', function (e) {
        $('#modal_insert_category').modal('show');
        $(document).on('submit', '#form_insert_category', function (e) {
            e.preventDefault();
            var categoryName = $('#category_name').val();
            var categoryDescription = $('#category_description').val();
            if (categoryName == '' || categoryDescription == '') {
                $('#insert_message').html('Please fiel in the blank').fadeIn().fadeOut(5000);
            } else if (categoryName.length < 2) {
                $('#insert_message').html('User Name Must least then 2 character').fadeIn().fadeOut(5000);
            } else {
                $.ajax({
                    method: 'POST',
                    url: "category_data.php",
                    data: {
                        insert_category: 'category',
                        category_name: categoryName,
                        category_description: categoryDescription,
                    },
                    success: function (response) {
                        $('#insert_message').html(response);
                        $('#form_insert_category').trigger('reset');
                        getData();

                    }
                });
            }

        });


    });



}


// update

function get_edit() {
    $(document).on('click', '#on_edit', function () {
        var id = $(this).attr('data-id');
        console.log(id)
        $.ajax({
            type: "post",
            url: "category_data.php",
            data: {
                get_edit_type: 'edit',
                up_id: id,
            },
            dataType: "JSON",
            success: function (data) {
                $('#up_id').val(data.category_id);
                $('#up_category_name').val(data.category_name);
                $('#up_category_description').val(data.category_description);
                $('#modal_update_category').modal('show');
                console.log(data.storag_id)


            }
        });

    });

    $(document).on('submit', '#form_update_category', function (e) {
        e.preventDefault();
        var up_category_name = $('#up_category_name').val();
        var up_category_description = $('#up_category_description').val();
        var up_id = $('#up_id').val();

        if (up_category_name == '' || up_category_description == '') {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">Please fiel in the blank</p>');

        } else if (up_category_name.length < 2) {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">User Name Must least then 2 character</p>');

        } else {
            $.ajax({
                method: 'POST',
                url: "category_data.php",
                data: {
                    up_category_name: up_category_name,
                    up_category_description: up_category_description,
                    up_id: up_id,
                },
                success: function (response) {
                    $('#update_message').html(response);
                    $(this).trigger('reset');
                    getData();

                }
            });
        }


    });

}

// delete

function on_delete() {
    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');
        var getDeleteData = 'delete';
        $.ajax({
            url: "category_data.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                delete_data_type: getDeleteData,
                delete_id_type: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#delete_id').val(data.category_id);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.category_name + "</strong>?</h4>");
            }
        });

    });

    $('#btn_delete').click(function (e) {
        var idDelete = $('#delete_id').val();
        var on_delete = 'on_delete';
        $.ajax({
            method: 'POST',
            url: "category_data.php",
            data: {
                on_delete_category: on_delete,
                id_delete_type: idDelete,
            },
            success: function (response) {
                $('#delete_modal').modal('hide');
                getData();
            }
        });



    });
}