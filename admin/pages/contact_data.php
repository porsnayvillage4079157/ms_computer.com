<?php
require '../connection/config.php';
if (isset($_POST['table'])) {
    $queryData = "SELECT * FROM contact";
    $stetement = $con->query($queryData);
    $total_row = mysqli_num_rows($stetement);
    $table = '
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th width="15%">User Name</th>
            <th width="10%">Comment</th>
            <th width="20%">Action</th>
        </tr>
    </thead>
    ';
    if ($total_row > 0) {
        $r = 1;
        while ($row = $stetement->fetch_assoc()) {
            $table .= '
    <tbody>
        <tr>
            <td>' . $r . '</td>
            <td>' . $row["user_name"] . '</td>
            <td>' . $row["comment"] . '</td>
            <td>
                    <button type="button" data-id="' . $row["user_id"] . '" class="btn btn-danger btn-xs" id="on_delete"><i class="fa fa-trash fa-fw" aria-hidden="true"></i>Delete</button>
                </td>
            </tr>
        </tbody>';
            $r++;
        }
    } else {
        $table .= '<tr colspan="4" align="center">Data not found</tr>';
    }
    $table .= '</table>';
    echo $table;
}

if (isset($_POST['deleteData'])) {
    $getId = $_POST['deleteId'];
    $getData = "SELECT * FROM contact WHERE user_id=$getId";
    $executeData = mysqli_query($con, $getData);
    $deleteData = $executeData->fetch_assoc();
    echo json_encode($deleteData);
}

if (isset($_POST['on_delete'])) {
    $idDelete = $_POST['idDelete'];
    $scriptDelete = "DELETE FROM contact WHERE user_id=$idDelete";
    $executeDelete = mysqli_query($con, $scriptDelete);
}
