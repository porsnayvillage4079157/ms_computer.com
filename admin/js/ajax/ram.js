$(document).ready(function () {
    getData();
    addram();
    get_edit();
    on_delete();
});

// get data

function getData() {
    $.ajax({
        type: "post",
        url: "ram_data.php",
        data: { get_data: 'get_data' },
        success: function (data) {
            $('#ram_table').html(data);

        }
    });
}

// insert ram

function addram() {
    $(document).on('click', '#btn_ram', function (e) {
        $('#modal_insert_ram').modal('show');
        $(document).on('submit', '#form_insert_ram', function (e) {
            e.preventDefault();
            var ramName = $('#ram_name').val();
            var ramSize = $('#ram_size').val();
            var ramDescription = $('#ram_description');
            var ramImage = $('#ram_image').val();
            if (ramName == '' || ramImage == '' || ramDescription == '' || ramSize == '') {
                $('#insert_message').html('Please fiel in the blank').fadeIn().fadeOut(5000);
            } else if (ramName.length < 2) {
                $('#insert_message').html('User Name Must least then 2 character').fadeIn().fadeOut(5000);
            } else {
                $.ajax({
                    method: 'POST',
                    url: "ram_data.php",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('#insert_message').html(response).fadeIn().fadeOut(4000);
                        $('#form_insert_ram').trigger('reset');
                        getData();

                    }
                });
            }

        });


    });



}

//update

function get_edit() {
    $(document).on('click', '#on_edit', function () {
        var id = $(this).attr('data-id');
        console.log(id)
        $.ajax({
            type: "post",
            url: "ram_data.php",
            data: {
                get_edit: 'edit',
                up_id: id,
            },
            dataType: "JSON",
            success: function (data) {
                $('#up_id').val(data.ram_id);
                $('#up_ram_name').val(data.ram_name);
                $('#up_ram_description').val(data.ram_description);
                $('#image_display').html('<img  id="myImage" src="' + data.ram_image + '" alt="" width="150px" height="150px">');
                $('#old_image').val(data.ram_image);
                $('#up_ram_size').val(data.ram_size);
                $('#modal_update_ram').modal('show');


            }
        });

    });

    $(document).on('submit', '#form_update_ram', function (e) {
        e.preventDefault();
        var up_ram_name = $('#up_ram_name').val();
        var up_ram_description = $('#up_ram_description').val();
        var up_ram_size = $('#up_ram_size');

        if (up_ram_name == '' || up_ram_description == '' || up_ram_size == '') {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">Please fiel in the blank</p>');

        } else if (up_ram_name.length < 2) {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">User Name Must least then 2 character</p>');

        } else {
            $.ajax({
                method: 'POST',
                url: "ram_data.php",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#update_message').html(response).fadeIn().fadeOut(4000);
                    $(this).trigger('reset');
                    getData();

                }
            });
        }


    });

}


// Delete Data 

function on_delete() {
    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');
        var deleteData = 'delete';
        $.ajax({
            url: "ram_data.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                deleteData: deleteData,
                deleteId: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#delete_id').val(data.ram_id);
                $('#delete_image').val(data.ram_image);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.ram_name + "</strong>?</h4>");
            }
        });

    });

    $('#btn_delete').click(function (e) {
        var idDelete = $('#delete_id').val();
        var imageDelete = $('#delete_image').val();
        console.log(imageDelete)
        var on_delete = 'on_delete';
        $.ajax({
            method: 'POST',
            url: "ram_data.php",
            data: {
                on_delete: on_delete,
                idDelete: idDelete,
                imageDelete: imageDelete,
            },
            success: function (response) {
                $('#delete_modal').modal('hide');
                getData();
            }
        });



    });
}