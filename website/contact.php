<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Contact</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="./assets/css/contact.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


</head>

<body>
  <?php include 'include/navigation.php'; ?>
  <header>
    <div class="main mt-4">

    </div>
    <div class="container">
      <div class="row">
        <section class="contact col-md-6 col-lg-6">
          <div class="contect">
            <h2>Contact Us</h2>
          </div>
          <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
          <div class="text"></div>
          <h3>Address</h3>
          <p>Sang kat Toul sangkel khan Russey Keo Phnom Penh</p>
          <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
          <div class="text"></div>
          <h3>Phone</h3>
          <p>086 47 12 56/094 765 55 66</p>
          <div class="icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
          <div class="text"></div>
          <h3>Email</h3>
          <p>shopassignment@gmail.com</p>
        </section>
        <section class="col-md-6 col-lg-6 mt-5">
          <div class="container">
            <h2 class="text-center bg-warning" style="color: black;"> Contact</h2>
            <div class="row justify-content-center">
              <div class="col-12 col-md-12 col-lg-12 pb-5">


                <!--Form with header-->

                <form id="form_contact" method="post">
                  <div class="card border-primary rounded-0">
                    <div class="card-header p-0">
                      <div class="bg-info text-white text-center py-2">
                        <h3><i class="fa fa-envelope"></i> Contact Us</h3>
                        <small id="message" style="display: flex; justify-content:end"></small>
                        <p class="m-0">MS Computer Form Contact</p>
                      </div>
                    </div>
                    <div class="card-body p-3">

                      <!--Body-->
                      <div class="form-group">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fa fa-user text-info"></i></div>
                          </div>
                          <input type="text" class="form-control" name="user_name" id="nombre" name="nombre" placeholder="User Name" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fa fa-comment text-info"></i></div>
                          </div>
                          <textarea class="form-control" name="comment" placeholder="Tell Us" rows="5" columns="20" required></textarea>
                        </div>
                      </div>

                    </div>

                  </div>
                  <button type="submit" class=" btn-block btn btn-primary btn-rounded"><i class="fa fa-telegram fa-fw" aria-hidden="true"></i>Send</button>
                </form>

                <!--Form with header-->


              </div>
            </div>
          </div>

        </section>
      </div>
    </div>

  </header>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="./assets/js/script.js"></script>

</body>

</html>