<?php
require_once '../connection/config.php';
//Get Data Product

if (isset($_POST['get_brand_data'])) {
    $queryData = "SELECT * FROM brand";
    $stetement = $con->query($queryData);
    $total_row = mysqli_num_rows($stetement);
    $table = '
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="10%">Image</th>
                <th width="5%">Brand Name</th>
                <th width="20%">Brand Des</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        ';
    if ($total_row > 0) {
        $r = 1;
        while ($row = $stetement->fetch_assoc()) {
            $table .= '
        <tbody>
            <tr>
                <td>' . $r . '</td>
                <td>
                   <img src="' . ($row['brand_image']) . '" height="60" width="75" class="img-thumbnail" />
                </td>
                <td>' . $row["brand_name"] . '</td>
                <td>' . $row["brand_description"] . '</td>
                <td>
                    <button type="button" data-id="' . $row["brand_id"] . '" class="btn btn-warning btn-xs" id="on_edit"><i class="fa fa-edit fa-fw" aria-hidden="true"></i>Edit</button>
                    <button type="button" data-id="' . $row["brand_id"] . '" class="btn btn-danger btn-xs" id="on_delete"><i class="fa fa-trash fa-fw" aria-hidden="true"></i>Delete</button>
                </td>
            </tr>
        </tbody>';
            $r++;
        }
    } else {
        $table .= '<tr class="alert alert-danger" colspan="4" align="center">Data not found</tr>';
    }
    $table .= '</table>';
    echo $table;
}


// Insert Brand

if (isset($_POST['brand_name_insert'])) {
    $brand_name = $_POST['brand_name_insert'];
    $brand_description = $_POST['brand_description'];
    $image =  $_FILES['brand_image'];
    $location = 'upload/brand/' . basename($_FILES['brand_image']['name']);
    $extension = pathinfo($location, PATHINFO_EXTENSION);
    if ($extension != 'jpg' && $extension != 'jpag' && $extension != 'png') {
        echo 'Only jpg, jpag and png are allowed';
    } elseif ($_FILES['brand_image']['size'] > 2000000) {
        echo 'File is too big!';
    } elseif (file_exists($location)) {
        echo 'File is alreay uploaded!';
    } elseif (move_uploaded_file($_FILES['brand_image']['tmp_name'], $location)) {
        $insert = "INSERT INTO brand (brand_name,brand_description,brand_image) VALUES ('$brand_name','$brand_description','$location')";
        mysqli_query($con, $insert);
    } else {
        echo 'Sometiong went wrong!';
    }
}

// get data to inputbox

if (isset($_POST['get_edit'])) {
    $id = $_POST['up_id'];
    $selectData = "SELECT * FROM brand WHERE brand_id=$id";
    $execute = mysqli_query($con, $selectData);
    $data = $execute->fetch_assoc();
    echo json_encode($data);
}

// Update 

if (isset($_POST['up_brand_name'])) {
    $up_brand_name = $_POST['up_brand_name'];
    $deleteImage = $_POST['old_image'];
    $up_brand_description = $_POST['up_brand_description'];
    $up_id = $_POST['up_id'];
    $locationUpdate = 'upload/brand/' . basename($_FILES['up_image']['name']);
    $imageUpdate =  $_FILES['up_image'];
    $extensionUpdate = pathinfo($locationUpdate, PATHINFO_EXTENSION);
    if ($extensionUpdate != 'jpg' && $extensionUpdate != 'jpag' && $extensionUpdate != 'png') {
        echo 'Only jpg, jpag and png are allowed';
    } elseif ($_FILES['up_image']['size'] > 2000000) {
        echo 'File is too big!';
    } elseif (file_exists($locationUpdate)) {
        echo 'File is alreay uploaded!';
    } elseif (move_uploaded_file($_FILES['up_image']['tmp_name'], $locationUpdate)) {
        $update = "UPDATE brand SET brand_name='$up_brand_name',brand_description='$up_brand_description',brand_image='$locationUpdate' WHERE brand_id=$up_id";

        $result = mysqli_query($con, $update);
        if ($result) {
            unlink($deleteImage);
            echo 'Data save!';
        }
    } else {
        echo 'Sometiong went wrong!';
    }
}

// DELETE

if (isset($_POST['deleteData'])) {
    $getId = $_POST['deleteId'];
    $getData = "SELECT * FROM brand WHERE brand_id='$getId'";
    $executeData = mysqli_query($con, $getData);
    $deleteData = $executeData->fetch_assoc();
    echo json_encode($deleteData);
}

if (isset($_POST['on_delete'])) {
    $idDelete = $_POST['idDelete'];
    $pathImage = $_POST['imageDelete'];
    $scriptDelete = "DELETE FROM brand WHERE brand_id=$idDelete";
    $executeDelete = mysqli_query($con, $scriptDelete);
    unlink($pathImage);
}
