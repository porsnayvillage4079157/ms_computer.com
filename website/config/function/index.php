<?php
require './config/config.php';
function selectMe($colName, $table)
{
    $select = "SELECT DISTINCT $colName FROM $table ORDER BY $colName";
    return $select;
}


function getByBrand($data)
{
    global $con;
    $sql = "SELECT  products.id,products.product_image,products.product_name,products.screen_size,products.             product_price,os.os_name,brand.brand_name,processor.processor_name,ram.ram_name,ram.ram_size,storage_type.storage_type_name,storage.storage_size,category.category_name from products 
            LEFT JOIN brand ON brand.brand_id = products.brand_id 
            LEFT JOIN processor ON processor.processor_id = products.processor_id 
            LEFT JOIN ram ON ram.ram_id = products.ram_id 
            LEFT JOIN os ON os.os_id = products.os_id 
            LEFT JOIN category ON category.category_id = products.category_id 
            LEFT JOIN storage ON storage.storage_id = products.storage_id 
            LEFT JOIN storage_type ON storage_type.storage_type_id = storage.storage_type_id WHERE brand_name = '$data' LIMIT 3";
    return $sql;
}


function getImage()
{
    global $con;
    $sql = "SELECT product_image FROM products LIMIT 3";
    $execute = mysqli_query($con, $sql);
    return $execute;
}

function getColor($arr)
{
    global $con;
    $output = '<small class="text-center" style="display: flex;justify-content:center;">';
    foreach ($arr as $color) {
        $output .= '<div class="color" style="background:' . $color . '">' . $color . '</div>';
    }
    $output .= '</small>';
    echo $output;
}
