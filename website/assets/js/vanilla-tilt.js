
VanillaTilt.init(document.querySelectorAll(".container-fluid .row .sub .row .ex .card-group .card .card-img-top"), {
    max: 15,
    speed: 900,
    perspective: 700,
    glare: true,
    scale: 1.2,
    "max-glare": 0.2,
    transform: 1000,
});