<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Products</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <?php
    include 'config/config.php';
    require './config/function/index.php';
    include './include/navigation.php';
    ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-lg-3">
                <h5>Filter Product</h5>
                <hr>
                <h6 class="text-info">Select Brand</h6>
                <ul class="list-group">
                    <?php
                    $r = selectMe('brand_name', 'brand');
                    $excecuteMe = $con->query($r);
                    while ($row = $excecuteMe->fetch_assoc()) :
                    ?>
                        <li class="list-group-item">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input product_check <?php echo $row['brand_name'] ?>" id="brand" data-id="<?php echo $row['brand_name'] ?>" value="<?php echo $row['brand_name']; ?>"> <?php echo $row['brand_name']; ?>

                                </label>
                            </div>
                        </li>
                    <?php endwhile ?>
                    <!-- RAM -->
                    <h6 class="text-info">Select RAM</h6>
                    <?php
                    $r = selectMe('ram_size', 'ram');
                    $excecuteMe = $con->query($r);
                    while ($row = $excecuteMe->fetch_assoc()) :
                    ?>
                        <li class="list-group-item">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input product_check" id="ram" value="<?php echo $row['ram_size']; ?>"> <?php echo $row['ram_size']; ?>
                                </label>
                            </div>
                        </li>
                    <?php endwhile ?>
                    <h6 class="text-info">Select hdd</h6>
                    <?php
                    $r = selectMe('storage_size', 'storage');
                    $excecuteMe = $con->query($r);
                    while ($row = $excecuteMe->fetch_assoc()) :
                    ?>
                        <li class="list-group-item">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input product_check" id="hdd" value="<?php echo $row['storage_size']; ?>"> <?php echo $row['storage_size']; ?>
                                </label>
                            </div>
                        </li>
                    <?php endwhile ?>
                    <!-- screen size -->
                    <h6 class="text-info">Select Screen Size</h6>
                    <?php
                    $r = selectMe('screen_size', 'products');
                    $excecuteMe = $con->query($r);
                    while ($row = $excecuteMe->fetch_assoc()) :
                    ?>
                        <li class="list-group-item">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input product_check" id="screen_size" value="<?php echo $row['screen_size']; ?>"> <?php echo $row['screen_size']; ?>
                                </label>
                            </div>
                        </li>
                    <?php endwhile ?>
                    <h6 class="text-info">Select CPU</h6>
                    <?php
                    $r = selectMe('processor_name', 'processor');
                    $excecuteMe = $con->query($r);
                    while ($row = $excecuteMe->fetch_assoc()) :
                    ?>
                        <li class="list-group-item">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input product_check" id="processor" value="<?php echo $row['processor_name']; ?>"> <?php echo $row['processor_name']; ?>
                                </label>
                            </div>
                        </li>
                    <?php endwhile ?>

                    <h6 class="text-info">Select OS</h6>
                    <?php
                    $r = selectMe('os_name', 'os');
                    $excecuteMe = $con->query($r);
                    while ($row = $excecuteMe->fetch_assoc()) :
                    ?>
                        <li class="list-group-item">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input product_check" id="os" value="<?php echo $row['os_name']; ?>"> <?php echo $row['os_name']; ?>
                                </label>
                            </div>
                        </li>
                    <?php endwhile ?>
                </ul>

            </div>
            <div class="col-md-9 col-sm-12 col-lg-9 sub">
                <div class="row" id="result">
                    <?php
                    $num_per_page = 9;


                    if (isset($_GET["page"])) {
                        $page = $_GET["page"];
                    } else {
                        $page = 1;
                    }

                    $start_from = ($page - 1) * 9;

                    // $sql="select * from employees limit $start_from,$num_per_page";
                    // $rs_result=mysql_query($sql);


                    $sql = "SELECT  products.id,products.product_color,products.product_image,products.product_name,products.screen_size,products.product_price,os.os_name,brand.brand_name,processor.processor_name,ram.ram_name,ram.ram_size,storage_type.storage_type_name,storage.storage_size,category.category_name from products 
                LEFT JOIN brand ON brand.brand_id = products.brand_id 
                LEFT JOIN processor ON processor.processor_id = products.processor_id 
                LEFT JOIN ram ON ram.ram_id = products.ram_id 
                LEFT JOIN os ON os.os_id = products.os_id 
                LEFT JOIN category ON category.category_id = products.category_id 
                LEFT JOIN storage ON storage.storage_id = products.storage_id 
                LEFT JOIN storage_type ON storage_type.storage_type_id = storage.storage_type_id LIMIT $start_from,$num_per_page";

                    $result = $con->query($sql);
                    $i = 0;

                    while ($row = $result->fetch_assoc()) : ?>
                        <?php $colors = unserialize($row['product_color']);
                        $new_color = implode("", $colors);
                        $real_color = explode(",", $new_color);
                        ?>
                        <div class="col-md-6 col-sm-12 mb-2 col-lg-4 mt-2 ex">
                            <div class="card-group" id="card">
                                <div class="card border-primary bg-dark text-light">
                                    <img width="200px" height="250px" class="card-img-top" src="<?php echo "../admin/pages/" . $row['product_image']; ?>">
                                    <div class="card-body">
                                        <h5 class="card-title bg-info rounded p-1"><?= $row['product_name']; ?></h5>
                                        <p class="card-text">
                                        <h4 class="text-danger">Price: <?= number_format($row['product_price']) . '$'; ?></h4>
                                        Brand : <?= $row['brand_name'] ?> <br>
                                        RAM Name : <?= $row['ram_name']; ?><br>
                                        RAM Size : <?= $row['ram_size'] . 'GB' ?> <br>
                                        Hard Disk : <?= $row['storage_type_name'] . ' - ' . $row['storage_size'] . ' GB' ?><br>
                                        Processor : <?= $row['processor_name']; ?><br>
                                        Screen Size : <?= $row['screen_size'] . 'inch' ?><br>
                                        Operation System : <?= $row['os_name']; ?><br>

                                        </p>
                                    </div>
                                    <div class="card-footer ">
                                        <small class="text-center" style="display: flex;justify-content:center;">
                                            <?php foreach ($real_color as $color) : ?>
                                                <div style="background: <?= $color ?>;" class="color"></div>

                                            <?php endforeach; ?>



                                        </small>
                                    </div>

                                </div>





                            </div>


                        </div>
                        <?php $i++; ?>


                    <?php endwhile; ?>


                </div>

            </div>

        </div>


    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-lg-3"></div>
            <div class="col-md-9 col-sm-12 col-lg-9 tex-center">
                <?php
                $sql_script = "SELECT  products.id,products.product_image,products.product_name,products.screen_size,products.product_price,os.os_name,brand.brand_name,processor.processor_name,ram.ram_name,ram.ram_size,storage_type.storage_type_name,storage.storage_size,category.category_name from products 
    LEFT JOIN brand ON brand.brand_id = products.brand_id 
    LEFT JOIN processor ON processor.processor_id = products.processor_id 
    LEFT JOIN ram ON ram.ram_id = products.ram_id 
    LEFT JOIN os ON os.os_id = products.os_id 
    LEFT JOIN category ON category.category_id = products.category_id 
    LEFT JOIN storage ON storage.storage_id = products.storage_id 
    LEFT JOIN storage_type ON storage_type.storage_type_id = storage.storage_type_id";
                $excecute = mysqli_query($con, $sql_script);
                $res_result = $excecute->fetch_assoc();
                $total_records = $excecute->num_rows;
                $total_pages = ceil($total_records / $num_per_page);
                ?>
                <nav class="bg-dark" aria-label="...">
                    <ul class="pagination pagination-lg bg-dark" style="justify-content:center;">

                        <?php if ($page > 1) : ?>
                            <li class="page-item bg-dark  " style="border: none;">
                                <a class="page-link bg-dark" href="product_filter.php?page=<?php echo $page - 1 ?>" tabindex="-1">Previous</a>
                            </li>

                        <?php endif; ?>
                        <?php
                        $next = $total_pages;
                        for ($i = 1; $i <= $total_pages; $i++) :
                        ?>


                            <li class="page-item">
                                <a class="page-link bg-dark" href="product_filter.php?page=<?php echo $i ?>" tabindex="-1"><?php echo $i ?></a>
                            </li>
                        <?php endfor; ?>
                        <?php if ($i > $page) : ?>
                            <li class="page-item">
                                <a class="page-link bg-dark" href="product_filter.php?page=<?php echo $page + 1 ?>" tabindex="-1">Next</a>
                            </li>

                        <?php endif; ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-tilt/1.7.0/vanilla-tilt.min.js"></script>
    <script type="text/javascript" src="assets/js/vanilla-tilt.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>