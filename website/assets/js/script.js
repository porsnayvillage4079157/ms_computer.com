
$(document).ready(function () {
    contactUs();
    $('.product_check').click(function () {
        var action = 'data';
        var brand = get_filter_text('brand');
        var ram = get_filter_text('ram');
        var hdd = get_filter_text('hdd');
        var processor = get_filter_text('processor');
        var screen_size = get_filter_text('screen_size');
        var os = get_filter_text('os');

        $.ajax({
            url: "action.php",
            method: "POST",
            data: {
                action: action,
                brand: brand,
                ram: ram,
                hdd: hdd,
                processor: processor,
                screen_size: screen_size,
                os: os,
            },
            success: function (response) {
                $('#result').html(response);
            }
        });
    });



    function get_filter_text(text_id) {
        var filterData = [];
        $('#' + text_id + ':checked').each(function () {
            filterData.push($(this).val());

        });
        return filterData;
    }

    $(document).on('click', '.pagination li', function (e) {
        $(this).addClass('active');

    });

    clickFilter('asus', 'asus');
    clickFilter('apple', 'apple');
    clickFilter('dell', 'dell');
    clickFilter('msi', 'msi');


    function clickFilter(id_name, brand_name) {
        $(document).on('click', '#' + id_name, function () {
            $.ajax({
                type: "post",
                url: "product_filter.php",
                data: { brand_name: 'brand' },
                success: function (response) {
                    window.location.href = 'product_filter.php';
                }
            });

        });

    }

});



function contactUs() {
    $('#form_contact').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            type: "post",
            url: "contact_action.php",
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (response) {
                $('#message').html(response);
                $('#form_contact').trigger('reset');

            }
        });
    });
}

