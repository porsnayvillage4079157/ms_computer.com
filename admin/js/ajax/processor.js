$(document).ready(function () {
    getData();
    addProcessor();
    get_edit();
    on_delete();

});

function getData() {
    $.ajax({
        type: "post",
        url: "processor_data.php",
        data: { get_data: 'get_data' },
        success: function (data) {
            $('#processor_table').html(data);

        }
    });
}

// Add Processor
function addProcessor() {
    $(document).on('click', '#btn_processor', function (e) {
        $('#modal_insert_processor').modal('show');
        $(document).on('submit', '#form_insert_processor', function (e) {
            e.preventDefault();
            var processorName = $('#processor_name').val();
            var processorImage = $('#processor_image').val();
            if (processorName == '' || processorImage == '') {
                $('#insert_message').html('Please fiel in the blank').fadeIn().fadeOut(5000);
            } else if (processorName.length < 2) {
                $('#insert_message').html('User Name Must least then 2 character').fadeIn().fadeOut(5000);
            } else {
                $.ajax({
                    method: 'POST',
                    url: "processor_data.php",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('#insert_message').html(response).fadeIn().fadeOut(4000);
                        $('#form_insert_processor').trigger('reset');
                        getData();

                    }
                });
            }

        });


    });



}

// Get Edit

function get_edit() {
    $(document).on('click', '#on_edit', function () {
        var id = $(this).attr('data-id');
        console.log(id)
        $.ajax({
            type: "post",
            url: "processor_data.php",
            data: {
                get_edit: 'edit',
                up_id: id,
            },
            dataType: "JSON",
            success: function (data) {
                $('#up_id').val(data.processor_id);
                $('#up_processor_name').val(data.processor_name);
                $('#up_processor_description').val(data.processor_description);
                $('#image_display').html('<img  id="myImage" src="' + data.processor_image + '" alt="" width="150px" height="150px">');
                $('#old_image').val(data.processor_image);
                $('#modal_update_processor').modal('show');


            }
        });

    });

    $(document).on('submit', '#form_update_processor', function (e) {
        e.preventDefault();
        var up_processor_name = $('#up_processor_name').val();
        var up_processor_description = $('#up_processor_description').val();

        if (up_processor_name == '') {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">Please fiel in the blank</p>');

        } else if (up_processor_name.length < 2) {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">User Name Must least then 2 character</p>');

        } else {
            $.ajax({
                method: 'POST',
                url: "processor_data.php",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#update_message').html(response).fadeIn().fadeOut(4000);
                    $(this).trigger('reset');
                    getData();

                }
            });
        }


    });

}

// DELETE 

function on_delete() {
    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');
        var deleteData = 'delete';
        $.ajax({
            url: "processor_data.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                deleteData: deleteData,
                deleteId: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#delete_id').val(data.processor_id);
                $('#delete_image').val(data.processor_image);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.processor_name + "</strong>?</h4>");
            }
        });

    });

    $('#btn_delete').click(function (e) {
        var idDelete = $('#delete_id').val();
        var imageDelete = $('#delete_image').val();
        console.log(imageDelete)
        var on_delete = 'on_delete';
        $.ajax({
            method: 'POST',
            url: "processor_data.php",
            data: {
                on_delete: on_delete,
                idDelete: idDelete,
                imageDelete: imageDelete,
            },
            success: function (response) {
                $('#delete_modal').modal('hide');
                getData();
            }
        });



    });
}