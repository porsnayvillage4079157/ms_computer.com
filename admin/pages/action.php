<?php
require_once '../connection/config.php';
if (isset($_POST['insert_product_to_db'])) {
    $category = $_POST['category'];
    $brand = $_POST['brand'];
    $productName = $_POST['product_name'];
    $ram = $_POST['ram'];
    $processor = $_POST['processor'];
    $storage = $_POST['storage'];
    $user = $_POST['admin_id'];
    $price = $_POST['price'];
    $screenSize = $_POST['screen_size'];
    $os = $_POST['os'];
    $product_color = serialize($_POST['color']);
    $image =  $_FILES['image'];
    $location = 'upload/product/' . basename($_FILES['image']['name']);
    $extension = pathinfo($location, PATHINFO_EXTENSION);
    if ($extension != 'jpg' && $extension != 'jpag' && $extension != 'png') {
        echo 'Only jpg, jpag and png are allowed';
    } elseif ($_FILES['image']['size'] > 2000000) {
        echo 'File is too big!';
    } elseif (file_exists($location)) {
        echo 'File is alreay uploaded!';
    } elseif (move_uploaded_file($_FILES['image']['tmp_name'], $location)) {
        $insert = "INSERT INTO products (brand_id,category_id,user_id,product_name,product_price,ram_id,storage_id,processor_id,screen_size,os_id,product_color,product_image) VALUES ($brand,$category,$user,'$productName','$price','$ram',$storage,$processor,'$screenSize',$os,'$product_color','$location')";
        $insertSuccessfully = mysqli_query($con, $insert);
        if ($insertSuccessfully) {
            echo 'Data Save';
        } else {
            echo 'Something went wrong';
        }
    }
}

// On Get Record by ID

if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $selectData = "SELECT * FROM products WHERE id=$id";
    $execute = mysqli_query($con, $selectData);
    $data = $execute->fetch_assoc();
    foreach ($execute as $item) {
        $colorss = unserialize($item['product_color']);
    }
    $new_colors = implode("", $colorss);
    $real_colors = explode(",", $new_colors);
    echo json_encode([$data, $real_colors]);
}

// on UPDATE Record

if (isset($_POST['up_product_name'])) {
    $up_brand = $_POST['up_brand'];
    $up_catecory = $_POST['up_category'];
    $up_product_name = $_POST['up_product_name'];
    $up_ram = $_POST['up_ram'];
    $up_user_id = $_POST['up_user_id'];
    $up_processor = $_POST['up_processor'];
    $up_storage = $_POST['up_storage'];
    $up_price = $_POST['up_price'];
    $up_screen_size = $_POST['up_screen_size'];
    $up_os = $_POST['up_os'];
    $deleteImage = $_POST['image_delete'];
    $up_id = $_POST['up_id'];
    $color_edit = serialize($_POST['colors']);
    $locationUpdate = 'upload/product/' . basename($_FILES['up_image']['name']);
    $imageUpdate =  $_FILES['up_image'];
    $extensionUpdate = pathinfo($locationUpdate, PATHINFO_EXTENSION);
    if ($extensionUpdate != 'jpg' && $extensionUpdate != 'jpag' && $extensionUpdate != 'png') {
        echo 'Only jpg, jpag and png are allowed';
    } elseif ($_FILES['up_image']['size'] > 2000000) {
        echo 'File is too big!';
    } elseif (file_exists($locationUpdate)) {
        echo 'File is alreay uploaded!';
    } elseif (move_uploaded_file($_FILES['up_image']['tmp_name'], $locationUpdate)) {
        $locationUpdate = 'upload/product/' . basename($_FILES['up_image']['name']);
        $update = "UPDATE products SET category_id=$up_catecory,brand_id='$up_brand',product_name='$up_product_name',ram_id='$up_ram',processor_id='$up_processor',storage_id='$up_storage',product_price='$up_price',screen_size='$up_screen_size',os_id='$up_os',product_color='$color_edit',product_image='$locationUpdate' WHERE id=$up_id";
        unlink($deleteImage);
        mysqli_query($con, $update);
        echo '<p class="text-primary">Data save!</p>';
    } else {
        echo 'Sometiong went wrong!';
    }
}

// Get Data To Delete

if (isset($_POST['deleteData'])) {
    $getId = $_POST['deleteId'];
    $getData = "SELECT * FROM products WHERE id='$getId'";
    $executeData = mysqli_query($con, $getData);
    $deleteData = $executeData->fetch_assoc();
    echo json_encode($deleteData);
}

// Delete

if (isset($_POST['on_delete'])) {
    $idDelete = $_POST['idDelete'];
    $pathImage = $_POST['imageDelete'];
    $scriptDelete = "DELETE FROM products WHERE id=$idDelete";
    $executeDelete = mysqli_query($con, $scriptDelete);
    unlink($pathImage);
}

// Get Data Delete

if (isset($_POST['dataDeleteAll'])) {
    $getAllId = implode(",", $_POST['deleteAllId']);
    echo json_encode($getAllId);
}

//Start Delete Select Record

if (isset($_POST['startingDelete'])) {
    $selectId = $_POST['deleteIdAll'];
    $scriptSelectImage = "SELECT product_image FROM products WHERE id IN ($selectId)";
    $getImage = mysqli_query($con, $scriptSelectImage);
    $total_product = mysqli_num_rows($getImage);
    echo $total_product;
    while ($rowImage = $getImage->fetch_assoc()) {
        unlink($rowImage['product_image']);
    }
    $dataDeleteAll = "DELETE FROM products WHERE id IN($selectId)";
    $successDelete = mysqli_query($con, $dataDeleteAll);
    if ($successDelete) {
        echo 'Data Delete';
    } else {
        echo 'Fail Delete';
    }
}


// View Product

if (isset($_POST['view'])) {
    $view_id = $_POST['idView'];
    $view = "SELECT  products.id,tbl_user.user_name,products.product_color,products.product_image,products.product_name,products.screen_size,products.product_price,os.os_name,brand.brand_name,processor.processor_name,ram.ram_name,ram.ram_size,storage_type.storage_type_name,storage.storage_size,category.category_name from products 
    LEFT JOIN brand ON brand.brand_id = products.brand_id 
    LEFT JOIN processor ON processor.processor_id = products.processor_id 
    LEFT JOIN ram ON ram.ram_id = products.ram_id 
    LEFT JOIN os ON os.os_id = products.os_id 
    LEFT JOIN category ON category.category_id = products.category_id 
    LEFT JOIN tbl_user ON tbl_user.user_id = products.user_id 
    LEFT JOIN storage ON storage.storage_id = products.storage_id 
    LEFT JOIN storage_type ON storage_type.storage_type_id = storage.storage_type_id WHERE id=$view_id";
    $view_result = mysqli_query($con, $view);
    $view_product = $view_result->fetch_assoc();
    foreach ($view_result as $item) {
        $colors = unserialize($item['product_color']);
    }
    $new_color = implode("", $colors);
    $real_color = explode(",", $new_color);
    echo json_encode([$view_product, $real_color]);
}
