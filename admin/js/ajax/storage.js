$(document).ready(function () {
    getData();
    addstorage();
    get_edit();
    on_delete()

});

// get data

function getData() {
    $.ajax({
        type: "post",
        url: "storage_data.php",
        data: { get_data: 'get_data' },
        success: function (data) {
            $('#storage_table').html(data);

        }
    });
}


function addstorage() {
    selectBox('btn_storage', 'storage_name');
    $(document).on('click', '#btn_storage', function (e) {
        $('#modal_insert_storage').modal('show');
        $(document).on('submit', '#form_insert_storage', function (e) {
            e.preventDefault();
            var storageName = $('#storage_name').val();
            var storageSize = $('#storage_size').val();
            var storageDescription = $('#storage_description');
            var storageImage = $('#storage_image').val();
            if (storageName == '' || storageImage == '' || storageDescription == '' || storageSize == '') {
                $('#insert_message').html('Please fiel in the blank').fadeIn().fadeOut(5000);
            } else if (storageName.length == '') {
                $('#insert_message').html('User Name Must least then 2 character').fadeIn().fadeOut(5000);
            } else {
                $.ajax({
                    method: 'POST',
                    url: "storage_data.php",
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('#insert_message').html(response).fadeIn().fadeOut(4000);
                        $('#form_insert_storage').trigger('reset');
                        getData();

                    }
                });
            }

        });


    });



}

//update
function selectBox(data, id) {
    $('#' + data).click(function () {
        $.ajax({
            type: "post",
            url: "storage_data.php",
            data: { drowpdownData: 'data' },
            dataType: "JSON",
            success: function (data) {
                $('#' + id).html(data);
            }
        });

    })

}
function get_edit() {
    $.ajax({
        type: "post",
        url: "storage_data.php",
        data: { drowpdownData: 'data' },
        dataType: "JSON",
        success: function (data) {
            $('#up_storage_name').html(data);
        }
    });

    $(document).on('click', '#on_edit', function () {
        var id = $(this).attr('data-id');
        $.ajax({
            type: "post",
            url: "storage_data.php",
            data: {
                get_edit: 'edit',
                up_id: id,
            },
            dataType: "JSON",
            success: function (data) {
                $('#up_id').val(data.storage_id);
                $('#up_storage_size').val(data.storage_size);
                $('#up_storage_description').val(data.storage_description);
                $('#image_display').html('<img  id="myImage" src="' + data.storage_image + '" alt="" width="150px" height="150px">');
                $('#old_image').val(data.storage_image);
                $('#modal_update_storage').modal('show');
                $('#up_storage_name').val(data.storage_type_id)
                console.log(data.storage_type_id)


            }
        });

    });

    $(document).on('submit', '#form_update_storage', function (e) {
        e.preventDefault();
        var up_storage_name = $('#up_storage_name').val();
        var up_storage_description = $('#up_storage_description').val();
        var up_storage_size = $('#up_storage_size');

        if (up_storage_name == '' || up_storage_description == '' || up_storage_size == '') {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">Please fiel in the blank</p>');

        } else if (up_storage_name.length < 1) {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">User Name Must least then 2 character</p>');

        } else {
            $.ajax({
                method: 'POST',
                url: "storage_data.php",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#update_message').html(response).fadeIn().fadeOut(4000);
                    $(this).trigger('reset');
                    getData();

                }
            });
        }


    });

}


// Delete Data 

function on_delete() {
    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');
        var deleteData = 'delete';
        $.ajax({
            url: "storage_data.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                deleteData: deleteData,
                deleteId: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#delete_id').val(data.storage_id);
                $('#delete_image').val(data.storage_image);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.storage_type_name + '-' + data.storage_size + "GB</strong>?</h4>");
            }
        });

    });

    $('#btn_delete').click(function (e) {
        var idDelete = $('#delete_id').val();
        var imageDelete = $('#delete_image').val();
        console.log(imageDelete)
        var on_delete = 'on_delete';
        $.ajax({
            method: 'POST',
            url: "storage_data.php",
            data: {
                on_delete: on_delete,
                idDelete: idDelete,
                imageDelete: imageDelete,
            },
            success: function (response) {
                $('#delete_modal').modal('hide');
                getData();
            }
        });



    });
}