$(document).ready(function () {
    getData();
    addStorageType();
    get_edit();
    on_delete();

});


function getData() {
    $.ajax({
        type: "post",
        url: "storage_data.php",
        data: { get_data_type: 'get_data' },
        success: function (data) {
            $('#storage_type_table').html(data);

        }
    });
}

// add Strage type

function addStorageType() {
    $(document).on('click', '#btn_storage_type', function (e) {
        $('#modal_insert_storage_type').modal('show');
        $(document).on('submit', '#form_insert_storage_type', function (e) {
            e.preventDefault();
            var storageTypeName = $('#storage_type_name').val();
            var storageTypeDescription = $('#storage_type_description').val();
            if (storageTypeName == '' || storageTypeDescription == '') {
                $('#insert_message').html('Please fiel in the blank').fadeIn().fadeOut(5000);
            } else if (storageTypeName.length < 2) {
                $('#insert_message').html('User Name Must least then 2 character').fadeIn().fadeOut(5000);
            } else {
                $.ajax({
                    method: 'POST',
                    url: "storage_data.php",
                    data: {
                        insert_storage_type: 'storage',
                        storage_type_name: storageTypeName,
                        storage_type_description: storageTypeDescription,
                    },
                    success: function (response) {
                        $('#insert_message').html(response).fadeIn().fadeOut(4000);
                        $('#form_insert_storage_type').trigger('reset');
                        getData();

                    }
                });
            }

        });


    });



}


//update


function get_edit() {
    $(document).on('click', '#on_edit', function () {
        var id = $(this).attr('data-id');
        console.log(id)
        $.ajax({
            type: "post",
            url: "storage_data.php",
            data: {
                get_edit_type: 'edit',
                up_id: id,
            },
            dataType: "JSON",
            success: function (data) {
                $('#up_id').val(data.storage_type_id);
                $('#up_storage_type_name').val(data.storage_type_name);
                $('#up_storage_type_description').val(data.storage_type_description);
                $('#modal_update_storage_type').modal('show');
            }
        });

    });

    $(document).on('submit', '#form_update_storage_type', function (e) {
        e.preventDefault();
        var up_storage_type_name = $('#up_storage_type_name').val();
        var up_storage_type_description = $('#up_storage_type_description').val();
        var up_id = $('#up_id').val();

        if (up_storage_type_name == '' || up_storage_type_description == '') {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">Please fiel in the blank</p>').fadeIn().fadeOut(4000);

        } else if (up_storage_type_name.length < 2) {

            $('#update_message').html('<p id="update_message" class="alert-danger p-1">User Name Must least then 2 character</p>').fadeIn().fadeOut(4000);

        } else {
            $.ajax({
                method: 'POST',
                url: "storage_data.php",
                data: {
                    up_storage_type_name: up_storage_type_name,
                    up_storage_type_description: up_storage_type_description,
                    up_id: up_id,
                },
                success: function (response) {
                    $('#update_message').html(response).fadeIn().fadeOut(4000);
                    $(this).trigger('reset');
                    getData();

                }
            });
        }


    });

}


// delete storage_type

function on_delete() {
    $(document).on('click', '#on_delete', function () {
        var deleteId = $(this).attr('data-id');
        var getDeleteData = 'delete';
        $.ajax({
            url: "storage_data.php",
            method: 'post',
            dataType: 'JSON',
            data: {
                delete_data_type: getDeleteData,
                delete_id_type: deleteId,
            },
            success: function (data) {
                $('#delete_modal').modal('show');
                $('#delete_id').val(data.storage_type_id);
                $('#show_delete').html("<h4 class='mx-auto text-center'>Are you sure want to delete <strong>" + data.storage_type_name + "</strong>?</h4>");
            }
        });

    });

    $('#btn_delete').click(function (e) {
        var idDelete = $('#delete_id').val();
        var on_delete = 'on_delete';
        $.ajax({
            method: 'POST',
            url: "storage_data.php",
            data: {
                on_delete_storage_type: on_delete,
                id_delete_type: idDelete,
            },
            success: function (response) {
                $('#delete_modal').modal('hide');
                getData();
            }
        });



    });
}