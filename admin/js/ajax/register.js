$(document).ready(function () {
    $('#form_add_user').submit(function (e) {
        e.preventDefault();
        const name = $('#name').val();
        const email = $('#email').val();
        const password = $('#password').val();
        if (name == '') {
            $('.name').addClass('is-invalid');
        } else {
            $('.name').removeClass('is-invalid');
        }

        if (email == '') {
            $('.email').addClass('is-invalid');
        } else {
            $('.email').removeClass('is-invalid');
        }
        if (password == '') {
            $('.password').addClass('is-invalid');
        } else {
            $('.password').removeClass('is-invalid');
        }

        if (name != '' && email != '' && password != '') {
            $.ajax({
                method: 'POST',
                url: "signup.php",
                dataType: 'JSON',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    $('.message').html(response);
                    if (response.status === "error") {
                        $('.email').addClass('is-invalid');
                        $('.emailError').html(response.message);

                    } else if (response.status == 'success') {
                        window.location = 'forms.php';
                    } else if (response.messageError) {
                        $('#message').html('<p class="alert alert-danger">' + response.messageError + '</p>').fadeIn().fadeOut(5000);
                    } else {
                        $('.email').addClass('is-invalid');
                        $('.emailError').html(response.message);
                        $('#message').html('<small class="alert alert-danger">' + response.messageError + '</small>').fadeIn().fadeOut(5000);

                    }

                }
            });
        }

    });




    // User Login
    $('#login').click(function () {
        const email = $('#email').val();
        const password = $('#password').val();
        const name = $('#name').val();
        if (email == '') {
            $('.email').addClass('is-invalid');
        } else {
            $('.email').removeClass('is-invalid');
        }

        if (password == '') {
            $('.password').addClass('is-invalid');
        } else {
            $('.password').removeClass('is-invalid');
        }

        if (email != '' && password != '') {
            var action = 'login';
            $.ajax({
                type: "post",
                url: "signup.php",
                data: {
                    action: action,
                    log_email: email,
                    log_password: password,
                    log_name: name,
                },
                dataType: "JSON",
                success: function (response) {
                    console.log(response)
                    if (response.status == 'success') {
                        window.location = 'index.php';
                    } else if (response.status == 'errorPassword') {
                        $('.password').addClass('is-invalid');
                        $('.errorPassword').html(response.message);
                        $('.email').removeClass('is-invalid');
                        $('.errorEmail').html('');

                    } else {
                        $('.email').addClass('is-invalid');
                        $('.errorEmail').html(response.message);
                        $('.password').removeClass('is-invalid');
                        $('.errorPassword').html('');


                    }

                }
            });
        }
    });



});

